<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\Hotel\UsersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['prefix' => 'admin'], function () {
	Voyager::routes();
});

// Requests
Route::group(
	['middleware' => 'hotel.admin'],
	function () {

		// Main
		Route::get('/', [MainController::class, 'index'])->name('main');

		// Orders
		Route::resources(['orders' => Hotel\OrdersController::class], ['as' => 'hotel']);
		Route::get('/orders/restore/{id}', [\App\Http\Controllers\Hotel\OrdersController::class, 'restore'])->name('hotel.orders.restore');

		// Rooms
		Route::resources(['rooms' => Hotel\RoomsController::class], ['as' => 'hotel']);
		Route::get('/rooms/restore/{id}', [\App\Http\Controllers\Hotel\RoomsController::class, 'restore'])->name('hotel.rooms.restore');

		// Users
		Route::resources(['users' => Hotel\UsersController::class], ['as' => 'hotel']);
		Route::get('/search-user', [UsersController::class, 'searchUser'])->name('hotel.users.searchUser');

		// Guests
		Route::resources(['guests' => Hotel\GuestsController::class], ['as' => 'hotel']);
		Route::get('/guests/restore/{id}', [\App\Http\Controllers\Hotel\GuestsController::class, 'restore'])->name('hotel.guests.restore');

		// Services
		Route::resources(['services' => Hotel\ServicesController::class], ['as' => 'hotel']);
		Route::post('/services/update/{id}', [\App\Http\Controllers\Hotel\ServicesController::class, 'update'])->name('hotel.services.update');
		Route::get('/services/restore/{id}', [\App\Http\Controllers\Hotel\ServicesController::class, 'restore'])->name('hotel.services.restore');
		Route::post('/services/image-uploader', [\App\Http\Controllers\Hotel\ServicesController::class, 'imageUploader'])
			->name('hotel.services.imageUploader');
		Route::post('/services/image-delete', [\App\Http\Controllers\Hotel\ServicesController::class, 'destroyImage'])
			->name('hotel.services.destroyImage');
		Route::get('/services/archive/{id}/{value}', [\App\Http\Controllers\Hotel\ServicesController::class, 'archive'])
			->name('hotel.services.archive');

		// Offers
		Route::resources(['offers' => Hotel\OffersController::class], ['as' => 'hotel']);
		Route::post('/offers/update/{id}', [\App\Http\Controllers\Hotel\OffersController::class, 'update'])->name('hotel.offers.update');
		Route::get('/offers/archive/{id}/{value}', [\App\Http\Controllers\Hotel\OffersController::class, 'archive'])->name('hotel.offers.archive');
		Route::post('/offers/uploader/image-uploader', [\App\Http\Controllers\Hotel\OffersController::class, 'imageUploader'])
			->name('hotel.offers.imageUploader');
		Route::post('/offers/uploader/image-delete', [\App\Http\Controllers\Hotel\OffersController::class, 'destroyImage'])
			->name('hotel.offers.destroyImage');

		// News
		Route::resources(['news' => Hotel\NewsController::class], ['as' => 'hotel']);
		Route::get('/news/archive/{id}/{value}', [\App\Http\Controllers\Hotel\NewsController::class, 'archive'])->name('hotel.news.archive');

		// Articles
		Route::resources(['articles' => Hotel\ArticlesController::class], ['as' => 'hotel']);
		Route::get('/articles/archive/{id}/{value}', [\App\Http\Controllers\Hotel\ArticlesController::class, 'archive'])
			->name('hotel.articles.archive');
		Route::get('/hotel/contacts', [\App\Http\Controllers\Hotel\ArticlesController::class, 'contacts'])->name('hotel.contacts');
		Route::patch('/hotel/contacts', [\App\Http\Controllers\Hotel\ArticlesController::class, 'contactsUpdate'])->name('hotel.contacts.update');

		// Settings
		Route::group(['prefix' => 'settings'], function () {
			Route::get('/', [\App\Http\Controllers\Hotel\SettingsController::class, 'index'])->name('hotel.settings.index');
			Route::put('/', [\App\Http\Controllers\Hotel\SettingsController::class, 'update'])->name('hotel.settings.update');
			Route::get('/languages', [\App\Http\Controllers\Hotel\SettingsController::class, 'languages'])->name('hotel.settings.languages');
			Route::get('/currencies', [\App\Http\Controllers\Hotel\SettingsController::class, 'currencies'])->name('hotel.settings.currencies');
		});

		Route::get('/translation', [MainController::class, 'translation'])->name('translation');

		// Profile
		Route::group(['prefix' => 'profile'], function () {
			Route::get('/settings', [\App\Http\Controllers\User\Profile\SettingsController::class, 'index'])->name('profile.settings');
			Route::patch('/settings', [\App\Http\Controllers\User\Profile\SettingsController::class, 'update'])->name('profile.settings.update');
		});

		// Users
		Route::resources(['crm-users' => User\UsersController::class], ['as' => 'hotel']);
		Route::get('/crm-users/block/{id}', [\App\Http\Controllers\User\UsersController::class, 'blocked'])
			->name('hotel.users.block');
	}
);

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
	\UniSharp\LaravelFilemanager\Lfm::routes();
});
