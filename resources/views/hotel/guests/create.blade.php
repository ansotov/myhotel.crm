@extends('layouts.app')

@section('title')
    {{ __('titles.guests') }}: {{ __('titles.add') }}
@endsection

@section('content')
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_content">

                {!! Form::open(['method' => 'post', 'url' => route('hotel.guests.store')]) !!}

                <div class="form-group col-md-4">
                    {!! Form::label('room_id', __('rooms.number')) !!}
                    <select name="room_id" class="form-control">
                        @foreach($rooms as $k => $v)
                            <option
                                value="{{ $v->id }}">{{ $v->number }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-4">
                    {!! Form::label('user_id', __('tables.user')) !!}
                    {{--<select name="user_id" class="form-control sumoselect">
                        @foreach($users as $k => $v)
                            <option value="{{ $v->id }}">{{ $v->name }}</option>
                        @endforeach
                    </select>--}}

                    {!! Form::text('', null, ['id' => 'searchUser', 'class' => 'form-control', 'required' => true]) !!}
                    {!! Form::hidden('user_id', null, ['id' => 'searchUser', 'class' => 'form-control', 'required' => true]) !!}
                </div>

                <div class="form-group col-md-4">
                    {!! Form::label('user_id', __('tables.new-user-description')) !!}

                    <div class="clearfix"></div>
                    <a
                        href="javascript:void(0);" class="btn btn-primary popup-ajax-link"
                        data-url="{{ route('hotel.users.create') }}">{{ __('buttons.new-user') }}</a>
                </div>

                <div class="clearfix"></div>

                <div class="form-group col-md-4">
                    {!! Form::label('start_at', __('tables.start_at')) !!}
                    {!! Form::date('start_at', null, ['class' => 'form-control', 'required' => true]) !!}
                </div>

                <div class="form-group col-md-4">
                    {!! Form::label('end_at', __('tables.end_at')) !!}
                    {!! Form::date('end_at', null, ['class' => 'form-control', 'required' => true]) !!}
                </div>

                <div class="form-group col-md-12">
                    {!! Form::label('comment', __('tables.comment')) !!}
                    {!! Form::textarea('comment', null, ['class' => 'form-control ckeditor']) !!}
                </div>

                <div class="form-group col-md-4">
                    {{ Form::submit(__('buttons.save'), ['class' => 'btn btn-primary']) }}
                </div>
            </div>
        </div>
    </div>
@endsection
