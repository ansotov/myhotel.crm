@extends('layouts.app')

@section('title')
    {{ __('titles.guests') }}: {{ __('titles.edit') }}
@endsection

@section('content')
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_content">

                {!! Form::open(['method' => 'post', 'url' => route('hotel.guests.update', $item->id)]) !!}
                @method('PATCH')

                <div class="form-group col-md-4">
                    <div>
                        {!! Form::label('room_id', __('tables.number')) !!}
                        <div class="input-group">
                            {!! Form::text('room_id', $item->room->number, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                            <a href="" class="btn btn-info">
                                <i class="fa fa-key"></i>
                            </a>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div>
                        {!! Form::label('user_id', __('tables.guest')) !!}
                        <div class="input-group">
                            {!! Form::text('user_id', $item->user->name, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                            <a href="" class="btn btn-info">
                                <i class="fa fa-user"></i>
                            </a>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6" style="padding-left: 0">
                        {!! Form::label('start_at', __('tables.start_at')) !!}
                        {!! Form::date('start_at', $item->start_at, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group col-md-6" style="padding: 0 4px 0 0">
                        {!! Form::label('end_at', __('tables.end_at')) !!}
                        {!! Form::date('end_at', $item->end_at, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group col-md-8">

                    {{--<div class="form-group col-md-12">
                        {!! Form::label('room_id', __('tables.booked_dates')) !!}
                        <div class="input-group">
                            --}}{{--<div id='calendar'></div>--}}{{--
                        </div>
                    </div>--}}
                </div>


                    <div class="form-group col-md-12">
                        {!! Form::label('comment', __('tables.comment')) !!}
                        {!! Form::textarea('comment', $item->comment, ['class' => 'form-control ckeditor']) !!}
                    </div>

                    <div class="form-group col-md-4">
                        {{ Form::submit(__('buttons.save'), ['class' => 'btn btn-primary']) }}
                    </div>

            </div>
        </div>
    </div>
@endsection

{{--@section('footer_scripts')
    <link href="{{ asset('vendors/fullcalendar/dist/fullcalendar.min.css') }}" rel="stylesheet">
    <script src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('vendors/fullcalendar/dist/fullcalendar.min.js') }}"></script>
@endsection--}}
