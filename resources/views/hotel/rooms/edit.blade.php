@extends('layouts.app')

@section('title')
    {{ __('titles.rooms') }}: {{ __('titles.edit') }}
@endsection

@section('content')
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_content">

                {!! Form::open(['method' => 'post', 'url' => route('hotel.rooms.update', $item->id)]) !!}
                @method('PATCH')
                <div class="form-group col-md-4">
                    {!! Form::label('number', __('tables.number')) !!}
                    {!! Form::text('number', $item->number, ['class' => 'form-control']) !!}
                </div>

                <div class="clearfix"></div>

                <div class="form-group col-md-4">
                    {!! Form::label('type_id', __('tables.type')) !!}
                    <select name="type_id" class="form-control">
                        @foreach($types as $k => $v)
                            <option
                                value="{{ $v->id }}"{{ $item->type_id == $v->id ? ' selected' : false }}>{{ $v->data->title ?? $v->title }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="clearfix"></div>

                <div class="form-group col-md-12">
                    {!! Form::label('comment', __('tables.comment')) !!}
                    {!! Form::textarea('comment', $item->comment, ['class' => 'form-control ckeditor']) !!}
                </div>

                <div class="form-group col-md-12">
                    {!! Form::label('active', __('tables.active')) !!}
                    {!! Form::checkbox('active', null, $item->active) !!}
                </div>


                <div class="form-group col-md-12">
                    {{ Form::submit(__('buttons.save'), ['class' => 'btn btn-primary']) }}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
