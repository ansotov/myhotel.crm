@extends('layouts.app')

@section('title')
    {{ __('titles.rooms') }}
@endsection

@section('content')
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_content">
                <div class="filters">
                    <ul class="list-inline float-right">
                        <li class="list-inline-item">
                            <a
                                href="{{ route('hotel.rooms.create') }}" class="btn btn-outline-primary btn-sm" role="button"
                                aria-pressed="true">{{ __('buttons.add') }}</a>
                        </li>
                    </ul>
                    {{--<div class="form-group col-md-4">
                        <select class="form-control" onchange="if (this.value) window.location.href=this.value">
                            <option value="{{ route('hotel.rooms.index') }}">{{ __('forms.actives') }}</option>
                            <option
                                value="{{ route('hotel.rooms.index', ['trashed' => true]) }}"{{ request('trashed') ? ' selected' : false }}>{{ __('forms.with-trashed') }}</option>
                            <option
                                value="{{ route('hotel.rooms.index', ['onlyTrashed' => true]) }}"{{ request('onlyTrashed') ? ' selected' : false }}>{{ __('forms.only-trashed') }}</option>
                        </select>
                    </div>--}}
                </div>
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th scope="col" width="20">{{ __('tables.number') }}</th>
                        <th scope="col" width="250">{{ __('tables.type') }}</th>
                        <th scope="col" width="">{{ __('tables.comment') }}</th>
                        <th scope="col" width="120"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $k => $v)
                        <tr{{ $v->trashed() ? ' class=table-danger' : false }}>
                            <th scope="row">{{ $v->number }}</th>
                            <td>{{ $v->type->data->title ?? $v->type->title }}</td>
                            <td>{!! $v->comment !!}</td>
                            <td>

                                @if($v->trashed())
                                    <a
                                        href="{{ route('hotel.rooms.restore', ['id' => $v->id, 'params' => request()]) }}"
                                        class="btn btn-outline-primary btn-sm" role="button"
                                        aria-pressed="true"
                                        onclick="return confirm('{{ __('messages.are-you-sure') }}')"
                                        title="{{ __('buttons.restore') }}">
                                        <i class="fas fa-trash-restore"></i>
                                    </a>
                                @else
                                    <a
                                        href="{{ route('hotel.rooms.edit', $v) }}"
                                        class="btn btn-outline-primary btn-sm" role="button"
                                        aria-pressed="true"
                                        title="{{ __('buttons.edit') }}">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    {{--{!! Form::open(['method' => 'DELETE','route' => ['hotel.rooms.destroy', $v->id],'style'=>'display:inline']) !!}
                                    {!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => 'return confirm("' . __('messages.are-you-sure') . '")']) !!}
                                    {!! Form::close() !!}--}}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $items->links() }}
            </div>
        </div>
    </div>
@endsection
