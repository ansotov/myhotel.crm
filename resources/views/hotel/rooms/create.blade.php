@extends('layouts.app')

@section('title')
    {{ __('titles.rooms') }}: {{ __('titles.add') }}
@endsection

@section('content')
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_content">

                {!! Form::open(['method' => 'post', 'url' => route('hotel.rooms.store')]) !!}
                <div class="form-group col-md-8 col-sm-12">
                    {!! Form::label('number', __('tables.numbers')) !!} ({{ __('tables.numbers-notice') }})
                    {!! Form::text('items[0][numbers]', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group col-md-4 col-sm-12">
                    {!! Form::label('type_id', __('tables.type')) !!}
                    <select name="items[0][type_id]" class="form-control">
                        @foreach($types as $k => $v)
                            <option
                                value="{{ $v->id }}">{{ $v->data->title ?? $v->title }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="clearfix"></div>

                <div class="form-group col-md-12">
                    {{ Form::submit(__('buttons.save'), ['class' => 'btn btn-primary']) }}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
