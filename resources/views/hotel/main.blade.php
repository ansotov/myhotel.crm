@extends('layouts.app')

@section('content')
    <div class="col-md-8">
        <div class="x_panel">
            <div class="x_content">
                <div id="mainb" style="height:350px;"></div>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-sm-4  ">
        <div class="x_panel">
            <div class="x_content">
                <div id="echart_pie" style="height:350px;"></div>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-sm-4  ">
        <div class="x_panel">
            <div class="x_content">
                <div id="echart_sonar" style="height:370px;"></div>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-sm-4  ">
        <div class="x_panel">
            <div class="x_content">
                <div id="echart_pie2" style="height:350px;"></div>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-sm-4  ">
        <div class="x_panel">
            <div class="x_content">
                <div id="echart_donut" style="height:350px;"></div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')

    <!-- ECharts -->
    <script src="{{ asset('vendors/echarts/dist/echarts.min.js') }}"></script>
    <script src="{{ asset('vendors/echarts/map/js/world.js') }}"></script>
@endsection
