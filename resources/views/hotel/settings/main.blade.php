@extends('layouts.app')

@section('content')
    <div class="col-md-12 col-sm-12">

        @include('hotel.settings.partials.top-menu')

        {!! Form::open(['method' => 'put', 'url' => route('hotel.settings.update')]) !!}

        <div class="x_panel">
            <div class="x_title">
                <h2>
                    <small>{{ __('settings.main_information.description') }}</small>
                </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <div class="form-group col-md-4">
                    {!! Form::label('title', __('tables.title')) !!}
                    {!! Form::text('settings[title]', auth()->user()->hotel->title, ['class' => 'form-control', 'required' => true]) !!}
                </div>
                <div class="clearfix"></div>

                <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                    @foreach(auth()->user()->languages as $k => $v)
                        <li class="nav-item">
                            <a
                                href="#{{ $v->language->const }}"
                                class="nav-link{{ $v->language->const == auth()->user()->language->const ? ' active' : false }}"
                                id="{{ $v->language->const }}-tab" data-toggle="tab"
                                role="tab"
                                aria-controls="{{ $v->language->const }}"
                                aria-selected="{{ $k == 0 ? 'true' : 'false' }}">{{ $v->language->data ? $v->language->data->title : null }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content" id="myTabContent">

                    <div class="alert alert-info" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span
                            class="sr-only">Error:</span> {{ __('notifications.main-language', ['lang' => auth()->user()->language->title]) }}
                    </div>
                    @foreach(auth()->user()->languages as $k => $v)
                        @php $data = auth()->user()->hotel->data($v->language->id)->first(); @endphp
                        <div
                            class="tab-pane fade{{ $v->language->const == auth()->user()->language->const ? ' show active' : false }}"
                            id="{{ $v->language->const }}"
                            role="tabpanel"
                            aria-labelledby="{{ $v->language->const }}-tab">

                            <div class="clearfix"></div>

                            <div class="form-group col-md-12">
                                {!! Form::label('description', __('tables.description')) !!}
                                {!! Form::textarea('settings[' . $v->language->id . '][description]', $data && $data->description ? $data->description : null, ['class' => 'form-control ckeditor']) !!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>


        <div class="form-group col-md-12">
            {{ Form::submit(__('buttons.save'), ['class' => 'btn btn-primary']) }}
        </div>


        {!! Form::close() !!}
    </div>
@endsection
