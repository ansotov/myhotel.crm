<div class="x_panel">
    <div class="top_tiles">
        <div class="col-md-2">
            <a href="{{ route('hotel.settings.index') }}"{{ Route::current()->getName() == 'hotel.settings' ? ' class=green' : false }}>
                <h2>{{ __('settings.main') }}</h2>
            </a>
        </div>

        <div class="col-md-2">
            <a href="{{ route('hotel.contacts') }}"{{ Route::current()->getName() == 'hotel.contacts' ? ' class=green' : false }}>
                <h2>{{ __('settings.contacts') }}</h2>
            </a>
        </div>

        <div class="col-md-2 col-sm-4  tile_stats_count">
            <a href="{{ route('hotel.settings.languages') }}"{{ Route::current()->getName() == 'hotel.settings.languages' ? ' class=green' : false }}>
                <h2>{{ __('settings.languages') }}</h2>
            </a>
        </div>

        <div class="col-md-2 col-sm-4  tile_stats_count">
            <a href="{{ route('hotel.settings.currencies') }}"{{ Route::current()->getName() == 'hotel.settings.currencies' ? ' class=green' : false }}>
                <h2>{{ __('settings.currencies') }}</h2>
            </a>
        </div>
    </div>
</div>
