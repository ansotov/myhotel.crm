@extends('layouts.app')

@section('content')
    <div class="col-md-12 col-sm-12">

        @include('hotel.settings.partials.top-menu')

        {!! Form::open(['method' => 'put', 'url' => route('hotel.settings.update')]) !!}
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    <small>{{ __('settings.currencies.description') }}</small>
                </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="form-row">

                    @foreach($items as $k => $v)
                        <div class="form-group col-md-3">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input
                                        class="form-check-input" name="currencies[{{ $v->id }}]"
                                        {{ (in_array($v->id, $currencies->toArray())) ? ' checked' : false }}
                                        {{ ($v->required == true) ? ' disabled' : false }}
                                        type="checkbox"
                                        value="">
                                    {{ $v->currency }} ({{ $v->country }} - {{ $v->symbol }})
                                </label>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="form-group col-md-12">
            {{ Form::submit(__('buttons.save'), ['class' => 'btn btn-primary']) }}
        </div>

        {!! Form::close() !!}
    </div>
@endsection
