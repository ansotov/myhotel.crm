@extends('layouts.app')

@section('title')
    {{ __('titles.news') }}
@endsection

@section('content')
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_content">
                <div class="filters">
                    <ul class="list-inline float-right">
                        <li class="list-inline-item">
                            <a
                                href="{{ route('hotel.news.create') }}" class="btn btn-outline-primary btn-sm" role="button"
                                aria-pressed="true">{{ __('buttons.add') }}</a>
                        </li>
                    </ul>
                    <div class="form-group col-md-2">
                        <select class="form-control" onchange="if (this.value) window.location.href=this.value">
                            <option value="{{ route('hotel.news.index') }}">{{ __('forms.actives') }}</option>
                            <option
                                value="{{ route('hotel.news.index', ['archive' => true]) }}"{{ request('archive') ? ' selected' : false }}>{{ __('forms.archive') }}</option>
                        </select>
                    </div>
                </div>
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th scope="col" width="200">{{ __('tables.title') }}</th>
                        <th scope="col" width="">{{ __('tables.description') }}</th>
                        <th scope="col" width="100">{{ __('tables.active') }}</th>
                        <th scope="col" width="150"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $k => $v)
                        <tr{{ $v->trashed() ? ' class=table-danger' : false }}>
                            <td>{{ $v->data->title }}</td>
                            <td>{!! $v->data->description !!}</td>
                            <td>@if($v->active == 1){{ __('buttons.yes') }}@else{{ __('buttons.no') }}@endif</td>
                            <td>
                                <a
                                    href="{{ route('hotel.news.edit', $v) }}"
                                    class="btn btn-outline-primary btn-sm" role="button"
                                    aria-pressed="true"
                                    title="{{ __('buttons.edit') }}">
                                    <i class="fas fa-edit"></i>
                                </a>
                                @if($v->archive)
                                    <a
                                        href="{{ route('hotel.news.archive', ['id' => $v->id, 'value' => 0]) }}"
                                        class="btn btn-outline-primary btn-sm" role="button"
                                        aria-pressed="true"
                                        onclick="return confirm('{{ __('messages.are-you-sure') }}')"
                                        title="{{ __('buttons.restore') }}">
                                        <i class="fas fa-trash-restore"></i>
                                    </a>
                                @else
                                    <a
                                        href="{{ route('hotel.news.archive', ['id' => $v->id, 'value' => true]) }}"
                                        class="btn btn-outline-primary btn-sm" role="button"
                                        aria-pressed="true"
                                        onclick="return confirm('{{ __('messages.are-you-sure') }}')"
                                        title="{{ __('buttons.in-archive') }}">
                                        <i class="fas fa-archive"></i>
                                    </a>
                                @endif
                                {!! Form::open(['method' => 'DELETE','route' => ['hotel.news.destroy', $v->id],'style'=>'display:inline']) !!}
                                {!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => 'return confirm("' . __('messages.are-you-sure') . '")']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $items->links() }}
            </div>
        </div>
    </div>
@endsection
