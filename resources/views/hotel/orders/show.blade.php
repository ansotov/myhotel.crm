@extends('layouts.app')

@section('title')
    {{ __('titles.order') }}: {{ $item->slug }}
@endsection

@section('content')
    <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
            <div class="x_content">

                <div class="form-group col-md-12">
                    <b>{!! Form::label('active', __('tables.product')) !!}</b>
                    @foreach($item->products as $k => $v)
                        <ul>
                            <li>{{ __('orders.name') }}: {{ $v->service->data->title }}</li>
                            <li>{{ __('orders.price') }}: {{ $v->price }}{{ $v->currency->symbol }}
                                ({{ $v->currency->currency }})
                            </li>
                            <li>{{ __('orders.qty') }}: {{ $v->qty }}</li>
                        </ul>
                        <hr>
                    @endforeach
                </div>

                @if($item->comment)
                    <div class="form-group col-md-12">
                        <b>{!! Form::label('active', __('tables.comment')) !!}</b>
                        <div>{{ $item->comment }}</div>
                        <hr>
                    </div>
                @endif

                <div class="form-group col-md-2">
                    <b>{!! Form::label('active', __('tables.status')) !!}</b>
                    {!! Form::open(['method' => 'post', 'url' => route('hotel.orders.update', $item->id)]) !!}
                    @method('PATCH')
                    <select name="status_id" class="form-control sumoselect" onchange="this.form.submit();">
                        @foreach($statuses as $k => $v)
                            <option
                                value="{{ $v->id }}"{{ $v->id == $item->status_id ? ' selected' : null }}>{{ $v->data->title }}</option>
                        @endforeach
                    </select>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
