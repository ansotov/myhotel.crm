@extends('layouts.app')

@section('title')
	{{ __('titles.orders') }}
@endsection

@section('content')
	<div class="col-md-12 col-sm-12  ">
		<div class="x_panel">
			<div class="x_content">

				{{-- Filters --}}
				@include('hotel.orders.partials.filters')

				<table class="table table-striped table-hover table-bordered">
					<thead>
					<tr>
						<th scope="col" width="20">{{ __('tables.number') }}</th>
						<th scope="col" width="100">{{ __('tables.room-number') }}</th>
						<th scope="col" width="50">{{ __('tables.created_at') }}</th>
						<th scope="col" width="150">{{ __('tables.guest') }}</th>
						<th scope="col" width="250">{{ __('tables.products') }}</th>
						<th scope="col" width="250">{{ __('tables.comment') }}</th>
						<th scope="col" width="50">{{ __('tables.status') }}</th>
						<th scope="col" width="20"></th>
					</tr>
					</thead>
					<tbody>
					@foreach($items as $k => $v)
						<tr>
							<th scope="row">{{ $v->slug }}</th>
							<td>
								{!! $v->room ? "<b>" . __('rooms.number') . '</b>: ' . $v->room->number : null !!}
								{!! $v->room ? "<br><b>" . __('rooms.type') . '</b>: ' . $v->room->type->title: null !!}
							</td>
							<td>{{ $v->created_at->format('d.m.Y H:i') }}</span></td>
							<td>
								<b>{{ __('user.name') }}</b>
								: {{ $v->user->name }}
								{!! $v->user->email ? "<br><b>" . __('user.email') . '</b>: ' . $v->user->email : null !!}
								{!! $v->user->phone ? "<br><b>" . __('user.phone') . '</b>: ' . $v->user->phone : null !!}
							</td>
							<td>
								@foreach($v->products as $k2 => $v2)
									<b>{{ __('orders.name') }}</b>: {{ $v2->service->data->title }}
									<br>
									<b>{{ __('orders.price') }}</b>: {{ $v2->price }}{{ $v2->currency->symbol }}
									({{ $v2->currency->currency }})
									@if($v2->qty > 1)
										<br>
										<li>{{ __('orders.qty') }}: {{ $v2->qty }}</li>
									@endif
									@if($k2 > 0)
										<hr>
									@endif
								@endforeach
							</td>
							<td>{{ $v->comment }}</td>
							<td>
								<div class="form-group col-md-12">
                                    <span
	                                    style="padding: 3px 10px;display:block;font-weight: bold;background-color: {{ $v->status->css }}">{{ $v->status->data->title }}</span>
									{!! Form::open(['method' => 'post', 'url' => route('hotel.orders.update', $v->id)]) !!}
									@method('PATCH')
									<select
										name="status_id" class="form-control sumoselect" onchange="this.form.submit();">
										@foreach($statuses as $k2 => $v2)
											<option
												value="{{ $v2->id }}"{{ $v2->id == $v->status_id ? ' selected' : null }}>{{ $v2->data->title }}</option>
										@endforeach
									</select>
									{!! Form::close() !!}
								</div>
							</td>
							<td>
								<a
									href="{{ route('hotel.orders.show', $v) }}"
									class="btn btn-outline-primary btn-sm" role="button"
									aria-pressed="true"
									title="{{ __('buttons.show') }}">
									<i class="fas fa-book"></i>
								</a>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				{{ $items->links() }}
			</div>
		</div>
	</div>
@endsection
