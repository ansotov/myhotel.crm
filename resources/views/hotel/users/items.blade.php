@extends('layouts.app')

@section('title')
	{{ __('titles.crm-users') }}
@endsection

@section('content')
	<div class="col-md-12 col-sm-12  ">
		<div class="x_panel">
			<div class="x_content">
				<div class="filters">
					<ul class="list-inline float-right">
						<li class="list-inline-item">
							<a
								href="javascript:void(0)"
								data-url="{{ route('hotel.crm-users.create') }}"
								data-href="{{ url()->current() }}"
								class="btn btn-outline-primary btn-sm popup-ajax-link"
								role="button"
								aria-pressed="true">{{ __('buttons.add') }}</a>
						</li>
					</ul>
					{{--<div class="form-group col-md-2">
						 <select class="form-control" onchange="if (this.value) window.location.href=this.value">
							 <option value="{{ route('hotel.articles.index') }}">{{ __('forms.actives') }}</option>
							 <option
								 value="{{ route('hotel.articles.index', ['archive' => true]) }}"{{ request('archive') ? ' selected' : false }}>{{ __('forms.archive') }}</option>
						 </select>
					 </div>--}}
				</div>
				<table class="table table-striped table-hover table-bordered">
					<thead>
					<tr>
						<th scope="col" width="200">ID</th>
						<th scope="col" width="">{{ __('tables.name') }}</th>
						<th scope="col" width="">{{ __('tables.email') }}</th>
						<th scope="col" width="100">{{ __('tables.role') }}</th>
						<th scope="col" width="100">{{ __('tables.active') }}</th>
						<th scope="col" width="150"></th>
					</tr>
					</thead>
					<tbody>
					@foreach($items as $k => $v)
						<tr{{ !is_null($v->blocked_at) ? ' style=background-color:#dc354540' : null }}>
							<td>{{ $v->id }}</td>
							<td>{!! $v->name !!}</td>
							<td>{!! $v->email !!}</td>
							<td>{!! $v->crmRole->name !!}</td>
							<td>@if(is_null($v->blocked_at)){{ __('buttons.yes') }}@else{{ __('buttons.blocked') }}
								- {{ $v->blocked_at->format('d.m.Y H:i') }}@endif</td>
							<td>
								@if($v->id !== auth()->user()->id)
									@if(checkUserAccess('block') === true && $v->hotel_main != true)
										<a
											href="{{ route('hotel.users.block', ['id' => $v->id]) }}"
											class="btn btn-outline-primary btn-sm" role="button"
											aria-pressed="true"
											onclick="return confirm('{{ __('messages.are-you-sure') }}')"
											title="{{ is_null($v->blocked_at) ? __('buttons.block') : __('buttons.unblock') }}">
											<i class="fas {{ is_null($v->blocked_at) ? 'fa-arrow-down' : 'fa-arrow-up' }}"></i>
										</a>
									@endif
									@if(checkUserAccess() === true)
										{!! Form::open(['method' => 'DELETE','route' => ['hotel.users.destroy', $v->id],'style'=>'display:inline']) !!}
										{!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => 'return confirm("' . __('messages.are-you-sure') . '")']) !!}
										{!! Form::close() !!}
									@endif
								@endif
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				{{ $items->links() }}
			</div>
		</div>
	</div>
@endsection
