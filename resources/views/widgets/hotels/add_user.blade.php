<form method="POST" action="{{ route('register') }}" class="form-a ajax-send">
    @csrf
    <div class="row">
        <div class="form-group col-md-12">
            {!! Form::label('name', __('forms.name')) !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            @error('name')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
        <div class="form-group col-md-12">
            {!! Form::label('email', __('forms.email')) !!}
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
            @error('email')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
        <div class="form-group col-md-12">
            {!! Form::label('phone', __('forms.phone')) !!}
            {!! Form::email('phone', null, ['class' => 'form-control']) !!}
            @error('email')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
        <div class="form-group col-md-12">
            <label for="lang_id">{{ __('settings.language') }}</label>
            <select
                class="form-control" name="lang_id"
                id="lang_id">
                @foreach(resolve(\App\Http\Controllers\Controller::class)->mainLanguages() as $k => $v)
                    <option
                        value="{{ $v->id }}"{{ app()->getLocale() == $v->const ? ' selected' : false }}>{{ $v->data->title }}</option>
                @endforeach
            </select>

            @error('lang_id')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
        <div class="form-group col-md-12">
            {!! Form::label('password', __('forms.password')) !!}
            {!! Form::email('password', null, ['class' => 'form-control']) !!}
            @error('password')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
        <div class="form-group col-md-12">
            {!! Form::label('password_confirmation', __('forms.password_confirmation')) !!}
            {!! Form::email('password_confirmation', null, ['class' => 'form-control']) !!}
            @error('password_confirmation')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>

        <div class="form-group col-md-4">
            {{ Form::submit(__('buttons.add'), ['class' => 'btn btn-primary']) }}
        </div>
    </div>
</form>
