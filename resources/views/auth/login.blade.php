@extends('layouts.auth')

@section('content')
    <div>

        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    <form method="POST" action="{{ route('login') }}" class="form-a ajax-send">
                        <h1>{{ __('forms.login-form') }}</h1>
                        @csrf
                        <div class="row">
                            <div class="col-md-12 mb-2">
                                <div class="form-group">
                                    <input
                                        placeholder="{{ __('forms.email') }}"
                                        id="email" type="email"
                                        class="form-control form-control-lg form-control-a @error('email') is-invalid @enderror"
                                        name="email"
                                        value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-group">
                                    <input
                                        placeholder="{{ __('forms.password') }}"
                                        id="password" type="password"
                                        class="form-control form-control-lg form-control-a @error('password') is-invalid @enderror"
                                        name="password"
                                        value="" required autocomplete="password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-group">
                                    <label for="remember">
                                        <input
                                            type="checkbox" name="remember"
                                            id="remember" {{ old('remember') ? 'checked' : '' }}>&nbsp;

                                        {{ __('forms.remember') }}</label>
                                    @error('remember')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-b">
                                        {{ __('Login') }}
                                    </button>

                                    @if (Route::has('password.request'))
                                        <a
                                            class="btn btn-link"
                                            href="{{ route('password.request') }}">
                                            {{ __('forms.forgot-password') }}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="clearfix"></div>

                    <div class="separator">
                        <p class="change_link">{{ __('forms.not-user') }}
                            <a href="{{ route('register') }}" class="to_register">{{ __('forms.registration') }}</a>
                        </p>

                        <div class="clearfix"></div>
                        <br/>

                        <div>
                            <p>©2019-{{ date('Y') }} {{ __('main.full-copyright') }}</p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection()
