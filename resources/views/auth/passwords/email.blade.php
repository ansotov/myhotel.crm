@extends('layouts.auth')

@section('content')
    <div>

        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">

                    <form method="POST" action="{{ route('password.email') }}" class="form-a ajax-send">
                        <h1>{{ __('forms.reset-password') }}</h1>
                        @csrf
                        <div class="row">

                            <div class="col-md-12 mb-2">
                                <div class="form-group">
                                    <label for="email">{{ __('forms.email') }}</label>
                                    <input
                                        id="email" type="email"
                                        class="form-control form-control-lg form-control-a @error('email') is-invalid @enderror"
                                        name="email"
                                        value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-12 mb-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-b">
                                        {{ __('buttons.send') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="clearfix"></div>

                    <div class="separator">
                        <p class="change_link">
                            <a href="{{ route('login') }}" class="to_register">{{ __('forms.login') }}</a> |
                            <a href="{{ route('register') }}" class="to_register">{{ __('forms.registration') }}</a>
                        </p>

                        <div class="clearfix"></div>
                        <br/>

                        <div>
                            <p>©2019-{{ date('Y') }} {{ __('main.full-copyright') }}</p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection()
