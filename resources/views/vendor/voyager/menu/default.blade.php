
@php

    if (Voyager::translatable($items)) {
        $items = $items->load('translations');
    }

@endphp

@foreach ($items as $item)

    @php

        $originalItem = $item;
        if (Voyager::translatable($item)) {
            $item = $item->translate($options->locale);
        }

        $isActive = null;
        $styles = null;
        $icon = null;

        // Background Color or Color
        if (isset($options->color) && $options->color == true) {
            $styles = 'color:'.$item->color;
        }
        if (isset($options->background) && $options->background == true) {
            $styles = 'background-color:'.$item->color;
        }

        // Check if link is current
        if(url($item->link()) == url()->current()){
            $isActive = 'active ';
        }

        // Set Icon
        if(isset($item->icon_class)){
            $icon = '<i class="fa ' . $item->icon_class . '"></i>';
        }

    @endphp

    <li class="{{ $isActive }}{{ isset($options->class) ? ' ' . $options->class : false }}">
        <a href="{{ url($item->link()) }}"{{ isset($options->li_a_class) ? ' class=' . $options->li_a_class : false }} target="{{ $item->target }}">
            {!! $icon !!}
            <span>{{ __($item->title) }}</span>
        </a>
        @if(!$originalItem->children->isEmpty())
            @include('voyager::menu.default', ['items' => $originalItem->children, 'options' => $options])
        @endif
    </li>
@endforeach
