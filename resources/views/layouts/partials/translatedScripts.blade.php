<script type="text/javascript">
    $(document).ready(function () {
        $('.sumoselect').SumoSelect(
            {
                placeholder: '{{ __('sumoselect.select-here') }}',
                csvDispCount: 3,
                captionFormat: '{0} {{ __('sumoselect.selected') }}',
                captionFormatAllSelected: '{0} ',
                floatWidth: 400,
                forceCustomRendering: false,
                nativeOnDevice: ['Android', 'BlackBerry', 'iPhone', 'iPad', 'iPod', 'Opera Mini', 'IEMobile', 'Silk'],
                outputAsCSV: false,
                csvSepChar: ',',
                okCancelInMulti: false,
                isClickAwayOk: false,
                triggerChangeCombined: true,
                selectAll: false,
                search: true,
                searchText: '{{ __('sumoselect.search') }}',
                noMatch: '{{ __('sumoselect.no-matches') }} "{0}"',
                prefix: '',
                locale: ['{{ __('sumoselect.ok') }}', '{{ __('sumoselect.cancel') }}', '{{ __('sumoselect.select-all') }}'],
                up: false,
                showTitle: true
            }
        );
    });

    CKEDITOR.editorConfig = function (config) {
        config.language = '{{ app()->getLocale() }}';
        config.extraPlugins = 'uploadimage';
    };

    moment.locale('{{ app()->getLocale() }}');
    $('.dateRange').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: 'DD.MM.Y',
            separator: " - ",
            applyLabel: "{{ __('date-range.apply') }}",
            cancelLabel: "{{ __('date-range.cancel') }}",
            fromLabel: "{{ __('date-range.from') }}",
            toLabel: "{{ __('date-range.to') }}",
            customRangeLabel: "{{ __('date-range.custom') }}"
        },
        maxDate: moment()
    });

    $('input.dateRange').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD.MM.Y') + ' - ' + picker.endDate.format('DD.MM.Y'));
    });
</script>
