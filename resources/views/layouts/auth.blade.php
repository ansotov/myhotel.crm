<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Favicons -->
    <link href="{{ asset('front/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('front/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="{{ asset('front/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{ asset('front/lib/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/notify.min.js') }}"></script>

    <!-- Libraries CSS Files -->
    <link href="{{ asset('front/lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('front/lib/animate/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('front/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('front/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="{{ asset('front/css/app.min.css') }}" rel="stylesheet">
    <link href="{{ asset('front/css/jquery-confirm.min.css') }}" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('vendors/nprogress/nprogress.css') }}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{ asset('vendors/animate.css/animate.min.css') }}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet">
</head>

<body class="login">

<div id="preloader"></div>

@if (session('status'))
    <script type="text/javascript">
        $.notify("{{ session('status') }}", "info", {
            autoHide: false,
            autoHideDelay: 9000
        });
    </script>
@endif
@if (session('success'))
    <script type="text/javascript">
        $.notify("{{ session('success') }}", "success", {
            autoHide: false,
            autoHideDelay: 9000
        });
    </script>
@endif
@if ($errors->any())
    @foreach ($errors->all() as $error)
        <script type="text/javascript">
            $.notify("{{ $error }}", "error", {
                autoHide: false,
                autoHideDelay: 9000
            });
        </script>
    @endforeach
@endif

@yield('content')


<!-- JavaScript Libraries -->
<script src="{{ asset('front/lib/jquery/jquery-migrate.min.js') }}"></script>
<script src="{{ asset('front/lib/popper/popper.min.js') }}"></script>
<script src="{{ asset('front/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('front/lib/easing/easing.min.js') }}"></script>
<script src="{{ asset('front/lib/owlcarousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('front/lib/scrollreveal/scrollreveal.min.js') }}"></script>
<script src="{{ asset('front/js/jquery-confirm.min.js') }}"></script>
<!-- Contact Form JavaScript File -->
<script src="{{ asset('front/contactform/contactform.js') }}"></script>

<!-- Template Main Javascript File -->
<script type="text/javascript" src="{{ asset('front/js/functions.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/app.min.js') }}"></script>

@yield('footer_scripts')

</body>
</html>
