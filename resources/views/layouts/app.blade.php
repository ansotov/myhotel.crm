<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Scripts -->
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="{{ asset('js/notify.min.js') }}"></script>
	<script src="{{ asset('js/jquery.sumoselect.min.js') }}" defer></script>

	<script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
	<script src="{{ asset('js/ckeditor/config.js') }}"></script>
	<script src="{{ asset('js/app.min.js') }}" defer></script>

	<!-- Styles -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
	<!-- Bootstrap -->
	<link href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
	<!-- NProgress -->
	<link href="{{ asset('vendors/nprogress/nprogress.css') }}" rel="stylesheet">
	<!-- iCheck -->
	<link href="{{ asset('vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">

	<link href="{{ asset('css/sumoselect.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/jquery.fancybox.css') }}" rel="stylesheet">
	<link href="{{ asset('css/jquery-ui.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/daterangepicker.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/app.min.css') }}" rel="stylesheet">
</head>


<body class="nav-md">

@if (session('status'))
	<script type="text/javascript">
        $.notify("{{ session('status') }}", "info", {
            autoHide: false,
            autoHideDelay: 9000
        });
	</script>
@endif
@if (session('success'))
	<script type="text/javascript">
        $.notify("{{ session('success') }}", "success", {
            autoHide: false,
            autoHideDelay: 9000
        });
	</script>
@endif
@if ($errors->any())
	@foreach ($errors->all() as $error)
		<script type="text/javascript">
            $.notify("{{ $error }}", "error", {
                autoHide: false,
                autoHideDelay: 9000
            });
		</script>
	@endforeach
@endif

<div class="container body">
	<div class="main_container">
		<div class="col-md-3 left_col">
			<div class="left_col scroll-view">
				<div class="navbar nav_title" style="border: 0;">
					<a href="{{ route('main') }}" class="site_title">
						<i class="fa fa-bar-chart"></i>
						<span>{{ config('app.name', 'Laravel') }}</span>
					</a>
				</div>

				<div class="clearfix"></div>

				<br/>

				<!-- sidebar menu -->
				<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
					<div class="menu_section">
						<ul class="nav side-menu">
							{{ menu('hotel', 'vendor/voyager/menu/main-left') }}
						</ul>
					</div>
				</div>
				<!-- /sidebar menu -->

			</div>
		</div>

		<!-- top navigation -->
		<div class="top_nav">
			<div class="nav_menu">
				<div class="nav toggle">
					<a id="menu_toggle">
						<i class="fa fa-bars"></i>
					</a>
				</div>
				<nav class="nav navbar-nav">
					<ul class=" navbar-right">
						<li class="nav-item dropdown open show" style="padding-left: 90px;">
							<a
								href="javascript:void(0);" class="user-profile dropdown-toggle" aria-haspopup="true"
								id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
								{{--<img src="{{ Voyager::image(Auth::user()->avatar) }}" alt="{{ auth()->user()->name }}">--}}
								{{ auth()->user()->name }}
							</a>
							<div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
								<a
									href="{{ route('profile.settings') }}" class="dropdown-item">
									{{ __('menu.settings') }}
								</a>
								<a
									class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
									if (confirm('{{ __('messages.are-you-sure') }}')) {return document.getElementById('logout-form').submit();}">
									<i class="fa fa-sign-out pull-right"></i>
									{{ __('menu.logout') }}
								</a>

								<form
									id="logout-form" action="{{ route('logout') }}" method="POST"
									style="display: none;">
									@csrf
								</form>
							</div>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3>@yield('title')</h3>
					</div>

					{{--<div class="title_right">
						<div class="col-md-5 col-sm-5   form-group pull-right top_search">
							<div class="input-group">
								<input
									type="text" class="form-control"
									placeholder="{{ __('buttons.search.placeholder') }}">
								<span class="input-group-btn">
								  <button class="btn btn-default" type="button">{{ __('buttons.search') }}</button>
								</span>
							</div>
						</div>
					</div>--}}
				</div>

				<div class="clearfix"></div>

				<div class="row">
					@yield('content')
				</div>
			</div>
		</div>
		<!-- /page content -->

		<!-- footer content -->
		<footer>
			<div class="pull-right">
				{{ __('main.copyright') }}
				@
				<a href="https://site-town.com" target="_blank">site-town.com</a>
			</div>
			<div class="clearfix"></div>
		</footer>
		<!-- /footer content -->
	</div>
</div>

<!-- jQuery -->
<script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/jquery.fancybox.js') }}"></script>
<script src="{{ asset('js/moment-with-locales.min.js') }}"></script>
<script src="{{ asset('js/daterangepicker.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('vendors/fastclick/lib/fastclick.js') }}"></script>
<!-- NProgress -->
<script src="{{ asset('vendors/nprogress/nprogress.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('vendors/iCheck/icheck.min.js') }}"></script>

@yield('footer_scripts')

@include('layouts.partials.translatedScripts')

</body>
</html>
