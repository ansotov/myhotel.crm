<?php 
return [
  'select-here' => 'Выберите здесь',
  'selected' => 'выбранный',
  'all-selected' => 'все выбрано!',
  'search' => 'Поиск...',
  'no-matches' => 'Нет совпадений для',
  'ok' => 'Хорошо',
  'cancel' => 'Отмена',
  'select-all' => 'Выбрать все',
];