<?php 
return [
  'language' => 'Язык',
  'languages' => 'Языки',
  'languages.description' => 'Выберите языки для вашей системы',
  'main_information' => 'Основная информация',
  'main_information.description' => 'Описание о вашем отеле',
  'main' => 'Основной',
  'currencies' => 'Валюты',
  'currencies.description' => 'Выберите валюту для вашей системы',
  'contacts' => 'Контакты',
  'contacts.description' => 'Контакты вашего отеля',
  'crm-users' => 'Пользователи CRM',
  'role' => 'Роль',
];