<?php 
return [
  'main' => 'Основной',
  'profile' => 'Профиль',
  'orders' => 'Заказы',
  'rooms' => 'Номера',
  'guests' => 'Гости',
  'services' => 'Услуги',
  'offers' => 'Акции',
  'news' => 'Новости',
  'articles' => 'Информация',
  'logout' => 'Выйти',
  'hotels' => 'Отели',
  'login' => 'Авторизоваться',
  'settings' => 'Настройки',
  'settings.hotel' => 'Настройки отеля',
];