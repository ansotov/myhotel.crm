<?php 
return [
  'rooms' => 'Номера',
  'add' => 'добавить',
  'edit' => 'редактировать',
  'hotel' => 'Гостиница',
  'guests' => 'Гости',
  'services' => 'Услуги',
  'offers' => 'Предложения',
  'news' => 'Новости',
  'articles' => 'Информация',
  'profile' => 'Профиль',
  'order' => 'Заказ',
  'orders' => 'Заказы',
  'contacts' => 'Контакты',
  'crm-users' => 'Пользователи CRM',
];