<?php 
return [
  'main-language' => 'Základní jazyk je :lang, pokud zůstane některá pole prázdná, bude přeložen s google překladačem, ale bez formátování textu. Pro lepší zobrazení byste měli ručně formátovat text.',
];