<?php 
return [
  'main' => 'Hlavní',
  'profile' => 'Profil',
  'orders' => 'Objednávky',
  'rooms' => 'Pokoje',
  'guests' => 'Hosté',
  'services' => 'Služby',
  'offers' => 'Nabídky',
  'news' => 'Zprávy',
  'articles' => 'Články',
  'logout' => 'Odhlásit se',
  'hotels' => 'Hotely',
  'login' => 'Přihlásit se',
  'settings' => 'Nastavení',
  'settings.hotel' => 'Nastavení hotelu',
];