<?php 
return [
  'language' => 'Jazyk',
  'languages' => 'Jazyky',
  'languages.description' => 'Vyberte jazyky pro svůj systém',
  'main_information' => 'Hlavní informace',
  'main_information.description' => 'Popis vašeho hotelu',
  'main' => 'Hlavní',
  'currencies' => 'Měny',
  'currencies.description' => 'Vyberte měny pro váš systém',
  'contacts' => 'Kontakty',
  'contacts.description' => 'Kontakty hotelu',
  'crm-users' => 'Uživatelé CRM',
  'role' => 'Role',
];