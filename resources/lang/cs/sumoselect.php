<?php 
return [
  'select-here' => 'Vyberte zde',
  'selected' => 'Vybraný',
  'all-selected' => 'všechny vybrané!',
  'search' => 'Vyhledávání...',
  'no-matches' => 'Žádné zápasy pro',
  'ok' => 'OK',
  'cancel' => 'zrušení',
  'select-all' => 'Vybrat vše',
];