<?php 
return [
  'main' => 'Main',
  'profile' => 'Profil',
  'orders' => 'Aufträge',
  'rooms' => 'Räume',
  'guests' => 'Gäste',
  'services' => 'Dienstleistungen',
  'offers' => 'Bietet an',
  'news' => 'Nachrichten',
  'articles' => 'Artikel',
  'logout' => 'Ausloggen',
  'hotels' => 'Hotels',
  'login' => 'Anmeldung',
  'settings' => 'Die einstellungen',
  'settings.hotel' => 'Hoteleinstellungen',
];