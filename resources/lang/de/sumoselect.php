<?php 
return [
  'select-here' => 'Wähle hier aus',
  'selected' => 'Ausgewählt',
  'all-selected' => 'Alle ausgewählt!',
  'search' => 'Suche...',
  'no-matches' => 'Keine Übereinstimmungen für',
  'ok' => 'OK',
  'cancel' => 'Stornieren',
  'select-all' => 'Wählen Sie Alle',
];