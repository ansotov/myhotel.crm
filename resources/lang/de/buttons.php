<?php 
return [
  'add' => 'Hinzufügen',
  'restore' => 'Wiederherstellen',
  'edit' => 'Bearbeiten',
  'delete' => 'Löschen',
  'reset' => 'Zurücksetzen',
  'save' => 'speichern',
  'search' => 'Suche',
  'search.placeholder' => 'Suchen nach...',
  'yes' => 'Ja',
  'no' => 'Nein',
  'send' => 'Senden',
  'in-archive' => 'Im archiv',
  'new-user' => 'Neuer Benutzer',
  'blocked' => 'verstopft',
  'block' => 'Block',
  'unblock' => 'Aufheben',
];