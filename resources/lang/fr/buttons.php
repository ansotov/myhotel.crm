<?php 
return [
  'add' => 'Ajouter',
  'restore' => 'Restaurer',
  'edit' => 'Éditer',
  'delete' => 'Supprimer',
  'reset' => 'Réinitialiser',
  'save' => 'Sauver',
  'search' => 'Chercher',
  'search.placeholder' => 'Rechercher...',
  'yes' => 'Oui',
  'no' => 'Non',
  'send' => 'Envoyer',
  'in-archive' => 'En archives',
  'new-user' => 'Nouvel utilisateur',
  'blocked' => 'Bloqué',
  'block' => 'Bloquer',
  'unblock' => 'Débloquer',
];