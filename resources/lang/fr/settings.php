<?php 
return [
  'language' => 'Langue',
  'languages' => 'Les langues',
  'languages.description' => 'Choisissez les langues pour votre système',
  'main_information' => 'Informations principales',
  'main_information.description' => 'Description de votre hôtel',
  'main' => 'Principale',
  'currencies' => 'Devises',
  'currencies.description' => 'Choisissez des devises pour votre système',
  'contacts' => 'Contacts',
  'contacts.description' => 'Contacts de votre hôtel',
  'crm-users' => 'Utilisateurs de la CRM',
  'role' => 'Rôle',
];