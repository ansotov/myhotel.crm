<?php

return [
	'language'                     => 'Language',
	'languages'                    => 'Languages',
	'languages.description'        => 'Choose languages for your system',
	'main_information'             => 'Main information',
	'main_information.description' => 'Description about your hotel',
	'main'                         => 'Main',
	'currencies'                   => 'Currencies',
	'currencies.description'       => 'Choose currencies for your system',
	'contacts'                     => 'Contacts',
	'contacts.description'         => 'Contacts of your hotel',
	'crm-users'                    => 'Crm users',
	'role'                         => 'Role',
];
