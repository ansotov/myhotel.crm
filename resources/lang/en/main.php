<?php

return [
    'menu'           => 'Menu',
    'welcome'        => 'Welcome',
    'copyright'      => 'Created by',
    'full-copyright' => 'All Rights Reserved. Hotel rooms CRM',
];
