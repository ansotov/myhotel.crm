<?php

return [
    'main'           => 'Main',
    'profile'        => 'Profile',
    'orders'         => 'Orders',
    'rooms'          => 'Rooms',
    'guests'         => 'Guests',
    'services'       => 'Services',
    'offers'         => 'Offers',
    'news'           => 'News',
    'articles'       => 'Articles',
    'logout'         => 'Logout',
    'hotels'         => 'Hotels',
    'login'          => 'Login',
    'settings'       => 'Settings',
    'settings.hotel' => 'Hotel settings',
];
