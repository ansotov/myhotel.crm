<?php

return [
	'add'                => 'Add',
	'restore'            => 'Restore',
	'edit'               => 'Edit',
	'delete'             => 'Delete',
	'reset'              => 'Reset',
	'save'               => 'Save',
	'search'             => 'Search',
	'search.placeholder' => 'Search for...',
	'yes'                => 'Yes',
	'no'                 => 'No',
	'send'               => 'Send',
	'in-archive'         => 'In archive',
	'new-user'           => 'New user',
	'blocked'            => 'Blocked',
	'block'              => 'Block',
	'unblock'            => 'Unblock',
];
