<?php 
return [
  'add' => 'Inserisci',
  'restore' => 'Ristabilire',
  'edit' => 'Modificare',
  'delete' => 'Elimina',
  'reset' => 'Ripristina',
  'save' => 'Salva',
  'search' => 'Ricerca',
  'search.placeholder' => 'Cercare...',
  'yes' => 'Sì',
  'no' => 'No',
  'send' => 'Spedire',
  'in-archive' => 'In archivio',
  'new-user' => 'Nuovo utente',
  'blocked' => 'Bloccato',
  'block' => 'Bloccare',
  'unblock' => 'Sbloccare',
];