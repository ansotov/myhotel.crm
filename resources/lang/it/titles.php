<?php 
return [
  'rooms' => 'Camere',
  'add' => 'Inserisci',
  'edit' => 'Modificare',
  'hotel' => 'Hotel',
  'guests' => 'Ospiti',
  'services' => 'Servizi',
  'offers' => 'Offerte',
  'news' => 'Notizia',
  'articles' => 'Articoli',
  'profile' => 'Profilo',
  'order' => 'Ordine',
  'orders' => 'Ordini',
  'contacts' => 'Contatti',
  'crm-users' => 'Utenti CRM.',
];