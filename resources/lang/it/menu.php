<?php 
return [
  'main' => 'Principale',
  'profile' => 'Profilo',
  'orders' => 'Ordini',
  'rooms' => 'Camere',
  'guests' => 'Ospiti',
  'services' => 'Servizi',
  'offers' => 'Offerte',
  'news' => 'Notizia',
  'articles' => 'Articoli',
  'logout' => 'Disconnettersi',
  'hotels' => 'Alberghi',
  'login' => 'Accesso',
  'settings' => 'Impostazioni',
  'settings.hotel' => 'Impostazioni dell\'hotel',
];