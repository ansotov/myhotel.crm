<?php 
return [
  'language' => 'Linguaggio',
  'languages' => 'Le lingue',
  'languages.description' => 'Scegli le lingue per il tuo sistema',
  'main_information' => 'Informazione principale',
  'main_information.description' => 'Descrizione del tuo hotel',
  'main' => 'Principale',
  'currencies' => 'Valute',
  'currencies.description' => 'Scegli le valute per il tuo sistema',
  'contacts' => 'Contatti',
  'contacts.description' => 'Contatti del tuo hotel',
  'crm-users' => 'Utenti CRM.',
  'role' => 'Ruolo',
];