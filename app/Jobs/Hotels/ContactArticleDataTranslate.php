<?php
/**
 * Class ContactArticleDataTranslate
 * Translator for hotel contacts
 */
namespace App\Jobs\Hotels;

use App\Models\Articles\ContactArticle;
use App\Models\Articles\ContactArticleData;
use App\Traits\Languages;
use App\Traits\Logs\Data;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

/**
 * Class ContactArticleDataTranslate
 *
 * @package App\Jobs\Hotels
 */
class ContactArticleDataTranslate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Languages, Data;

    /**
     * @var int
     */
    public $tries = 5;


    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * @var \App\Models\Articles\ContactArticle
     */
    private $item;

    /**
     * @var \App\Models\User
     */
    private $user;

    /**
     * @var string
     */
    private $fromLang;


    /**
     * ContactArticleDataTranslate constructor.
     *
     * @param \App\Models\Articles\ContactArticle $item
     * @param                                     $request
     * @param                                     $fromLang
     * @param                                     $user
     */
    public function __construct(ContactArticle $item, $request, $fromLang, $user)
    {
        $this->request  = $request;
        $this->item     = $item;
        $this->user     = $user;
        $this->fromLang = $fromLang;
    }

    /**
     * @throws \ErrorException
     */
    public function handle()
    {
        foreach ($this->hotelLanguages($this->item->hotel_id) as $v) {
            ContactArticleData::updateOrCreate(
                [
                    'article_id' => $this->item->id,
                    'lang_id'    => $v->lang_id,
                ],
                [
                    'text' => ! is_null($this->request['text'][$v->lang_id])
                        ? $this->request['text'][$v->lang_id]
                        : (! is_null($this->request['text'][$this->fromLang->id])
                            ? ucfirst(googleTrans(
                                $this->request['text'][$this->fromLang->id],
                                $v->language->const,
                                $this->fromLang->const
                            )) : ''),
                ]
            );
        }

        // Log data
        Log::info('Queue finished ', $this->dataArray($this->item, __CLASS__));
    }
}
