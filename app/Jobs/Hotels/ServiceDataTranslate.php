<?php

namespace App\Jobs\Hotels;

use App\Mail\TextMail;
use App\Models\Hotels\HotelService;
use App\Models\Hotels\HotelServiceData;
use App\Traits\Languages;
use App\Traits\Logs\Data;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

/**
 * Class ServiceDataTranslate
 *
 * @package App\Jobs\Hotels
 */
class ServiceDataTranslate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Languages, Data;

    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * @var \App\Models\Hotels\HotelService
     */
    private $item;

    /**
     * @var \App\Models\User
     */
    private $user;

    /**
     * @var string
     */
    private $fromLang;

    /**
     * @var int
     */
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(HotelService $item, $request, $fromLang, $user)
    {
        $this->request  = $request;
        $this->item     = $item;
        $this->user     = $user;
        $this->fromLang = $fromLang;
    }

    /**
     * @throws \ErrorException
     */
    public function handle()
    {
        foreach ($this->hotelLanguages($this->item->hotel_id) as $v) {

            HotelServiceData::updateOrCreate(
                [
                    'service_id' => $this->item->id,
                    'lang_id'    => $v->lang_id,
                ],
                [
					'title'       => !empty($this->request['title'][$v->lang_id])
						? $this->request['title'][$v->lang_id]
						: ucfirst(googleTrans(
							$this->request['title'][$this->fromLang->id],
							$v->language->const,
							$this->fromLang->const
						)),
                    'description' => !empty($this->request['description'][$v->lang_id])
                        ? $this->request['description'][$v->lang_id]
                        : (!is_null($this->request['description'][$this->fromLang->id])
                            ? ucfirst(googleTrans(
                                $this->request['description'][$this->fromLang->id],
                                $v->language->const,
                                $this->fromLang->const
                            )) : ''),
                ]
            );
        }

        // Log data
        Log::info('Queue finished ', $this->dataArray($this->item, __CLASS__));
    }
}
