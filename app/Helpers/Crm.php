<?php

use TCG\Voyager\Models\Role;

/**
 * Crm helpers
 */
if (! function_exists('crmRoles')) {
    /**
     * Custom path
     *
     * @return mixed
     */
    function crmRoles()
    {
        return Role::where(
            [
                'crm' => true,
            ]
        )->get();
    }
}
