<?php
/**
 * Created by site-town.com
 * User: ansotov
 * Date: 2020-05-13
 * Time: 17:55
 */

if ( ! function_exists('checkUserAccess')) {
	/**
	 * @return bool
	 */
	function checkUserAccess(string $type = 'delete'): bool
	{
		switch ($type) {
			case 'delete':
				if (
					auth()->user()->hotel_main != true ||
					auth()->user()->role->name != 'hotel_admin'
				) {
					return false;
				} else {
					return true;
				}
			case 'block':
				if (
					auth()->user()->role->name != 'hotel_admin'
				) {
					return false;
				} else {
					return true;
				}
		}

	}
}
