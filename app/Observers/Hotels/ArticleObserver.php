<?php

namespace App\Observers\Hotels;

use App\Models\Articles\Article;
use App\Models\Logs\Event;
use App\Models\Logs\EventType;
use Illuminate\Support\Facades\Log;

/**
 * Class ArticleObserver
 *
 * @package App\Observers\Hotels
 */
class ArticleObserver
{
    /**
     * Handle the article "created" event.
     *
     * @param \App\Models\Articles\Article $article
     *
     * @return void
     */
    public function created(Article $article)
    {
        Log::info('Created', $this->dataArray($article, __CLASS__));
        $event = new Event([
            'type_id'   => EventType::where(['const' => 'hotel_service_added'])->first()->id,
            'user_id'   => auth()->user()->id,
            'object_id' => $article->id,
        ]);
        $event->save();
    }

    /**
     * Handle the article "updated" event.
     *
     * @param \App\Models\Articles\Article $article
     *
     * @return void
     */
    public function updated(Article $article)
    {
        //
    }

    /**
     * Handle the article "deleted" event.
     *
     * @param \App\Models\Articles\Article $article
     *
     * @return void
     */
    public function deleted(Article $article)
    {
        //
    }

    /**
     * Handle the article "restored" event.
     *
     * @param \App\Models\Articles\Article $article
     *
     * @return void
     */
    public function restored(Article $article)
    {
        //
    }

    /**
     * Handle the article "force deleted" event.
     *
     * @param \App\Models\Articles\Article $article
     *
     * @return void
     */
    public function forceDeleted(Article $article)
    {
        //
    }
}
