<?php

namespace App\Observers\Hotels;

use App\Models\Rooms\Room;
use App\Traits\Logs\Data;
use Illuminate\Support\Facades\Log;

/**
 * Class RoomObserver
 *
 * @package App\Observers\Hotels
 */
class RoomObserver
{
    use Data;

    /**
     * Handle the room "created" event.
     *
     * @param \App\Models\Rooms\Room $room
     *
     * @return void
     */
    public function created(Room $room)
    {
        Log::info('Created', $this->dataArray($room, __CLASS__));
    }

    /**
     * Handle the room "updated" event.
     *
     * @param \App\Models\Rooms\Room $room
     *
     * @return void
     */
    public function updated(Room $room)
    {
        Log::info('Updated', $this->dataArray($room, __CLASS__));
    }

    /**
     * Handle the room "deleted" event.
     *
     * @param \App\Models\Rooms\Room $room
     *
     * @return void
     */
    public function deleted(Room $room)
    {
        Log::info('Deleted', $this->dataArray($room, __CLASS__));
    }

    /**
     * Handle the room "restored" event.
     *
     * @param \App\Models\Rooms\Room $room
     *
     * @return void
     */
    public function restored(Room $room)
    {
        Log::info('Restored', $this->dataArray($room, __CLASS__));
    }

    /**
     * Handle the room "force deleted" event.
     *
     * @param \App\Models\Rooms\Room $room
     *
     * @return void
     */
    public function forceDeleted(Room $room)
    {
        Log::info('Force deleted', $this->dataArray($room, __CLASS__));
    }
}
