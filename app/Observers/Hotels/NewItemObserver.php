<?php

namespace App\Observers\Hotels;

use App\Models\Logs\Event;
use App\Models\Logs\EventType;
use App\Models\News\NewItem;
use App\Traits\Logs\Data;
use Illuminate\Support\Facades\Log;

/**
 * Class NewItemObserver
 *
 * @package App\Observers\Hotels
 */
class NewItemObserver
{
    use Data;

    /**
     * Handle the new item "created" event.
     *
     * @param  \App\Models\News\NewItem  $newItem
     * @return void
     */
    public function created(NewItem $newItem)
    {
        Log::info('Created', $this->dataArray($newItem, __CLASS__));
        $event = new Event([
            'type_id'   => EventType::where(['const' => 'hotel_news_added'])->first()->id,
            'user_id'   => auth()->user()->id,
            'object_id' => $newItem->id,
        ]);
        $event->save();
    }

    /**
     * Handle the new item "updated" event.
     *
     * @param  \App\Models\News\NewItem  $newItem
     * @return void
     */
    public function updated(NewItem $newItem)
    {
        Log::info('Updated', $this->dataArray($newItem, __CLASS__));
        $event = new Event([
            'type_id'   => EventType::where(['const' => 'hotel_news_updated'])->first()->id,
            'user_id'   => auth()->user()->id,
            'object_id' => $newItem->id,
        ]);
        $event->save();
    }

    /**
     * Handle the new item "deleted" event.
     *
     * @param  \App\Models\News\NewItem  $newItem
     * @return void
     */
    public function deleted(NewItem $newItem)
    {
       //
    }

    /**
     * Handle the new item "restored" event.
     *
     * @param  \App\Models\News\NewItem  $newItem
     * @return void
     */
    public function restored(NewItem $newItem)
    {
        //
    }

    /**
     * Handle the new item "force deleted" event.
     *
     * @param  \App\Models\News\NewItem  $newItem
     * @return void
     */
    public function forceDeleted(NewItem $newItem)
    {
        //
    }
}
