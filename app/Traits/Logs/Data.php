<?php

namespace App\Traits\Logs;

/**
 * Trait Data
 *
 * @package App\Traits\Logs
 */
trait Data
{
    protected function dataArray($object, $class, $user = false): array
    {
        return [
            'class'   => $class,
            'table'   => $object->getTable(),
            'user_id' => auth()->user()->id ?? $user->id,
            'data'    => $object,
        ];
    }
}
