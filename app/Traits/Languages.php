<?php

namespace App\Traits;

use App\Models\Hotels\HotelLanguage;
use App\Models\Language;

/**
 * Trait Languages
 *
 * @package App\Traits
 */
trait Languages
{
    /**
     * @return \App\Models\Language[]|\Illuminate\Database\Eloquent\Collection
     */
    protected function languages()
    {
        return Language::all();
    }

    /**
     * @param bool $hotelId
     *
     * @return mixed
     */
    protected function hotelLanguages(bool $hotelId)
    {
        return HotelLanguage::whereHotelId($hotelId)->get();
    }
}
