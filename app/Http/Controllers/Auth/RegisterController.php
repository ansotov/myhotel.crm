<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Articles\ContactArticle;
use App\Models\Articles\ContactArticleData;
use App\Models\Hotels\Hotel;
use App\Models\Hotels\HotelCurrency;
use App\Models\Hotels\HotelData;
use App\Models\Hotels\HotelLanguage;
use App\Models\Permissions\CrmPermission;
use App\Models\Permissions\CrmPermissionDefault;
use App\Models\Permissions\CrmPermissionRole;
use App\Models\Permissions\CrmRole;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * Class RegisterController
 *
 * @package App\Http\Controllers\Auth
 */
class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/settings/languages';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param array $data
	 *
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data): \Illuminate\Contracts\Validation\Validator
	{
		return Validator::make($data, [
			'name'       => ['required', 'string', 'max:255'],
			'hotel_name' => ['required', 'string', 'max:255'],
			'email'      => ['required', 'string', 'email', 'max:255', 'unique:users'],
			'lang_id'    => ['required', 'integer'],
			'password'   => ['required', 'string', 'min:8', 'confirmed'],
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param array $data
	 *
	 * @return \App\Models\User
	 */
	protected function create(array $data): User
	{
		$hotel = Hotel::create();

		HotelData::create(
			[
				'lang_id'  => $data['lang_id'],
				'hotel_id' => $hotel->id,
				'title'    => $data['hotel_name'],
			]
		);

		foreach ($this->mainLanguages()->pluck('id') as $v) {
			HotelLanguage::create(
				[
					'hotel_id' => $hotel->id,
					'lang_id'  => $v,
				]
			);
		}

		foreach ($this->mainCurrencies()->pluck('id') as $v) {
			HotelCurrency::create(
				[
					'hotel_id'    => $hotel->id,
					'currency_id' => $v,
				]
			);
		}

		$contactArticle = ContactArticle::create(
			[
				'hotel_id' => $hotel->id,
			]
		);

		foreach ($hotel->languages as $v) {
			ContactArticleData::create(
				[
					'lang_id'    => $v->id,
					'article_id' => $contactArticle->id,
				]
			);
		}

		// Permissions
		$permissions = CrmPermissionDefault::all();
		$roles       = CrmRole::main(false)->get();
		foreach ($roles as $role) {
			foreach ($permissions as $permission) {
				CrmPermissionRole::create(
					[
						'hotel_id'      => $hotel->id,
						'permission_id' => $permission->permission_id,
						'role_id'       => $role->id,
					]
				);
			}
		}
		$mainPermissions = CrmPermission::all();
		$mainRoles       = CrmRole::main(true)->get();
		foreach ($mainRoles as $role) {
			foreach ($mainPermissions as $permission) {
				CrmPermissionRole::create(
					[
						'hotel_id'      => $hotel->id,
						'permission_id' => $permission->id,
						'role_id'       => $role->id,
					]
				);
			}
		}

		return User::create(
			[
				'name'       => $data['name'],
				'email'      => $data['email'],
				'hotel_id'   => $hotel->id,
				'role_id'    => $this->role('hotel_admin')->id,
				'crm_role_id'    => $this->crmRole('admin')->id,
				'hotel_main' => true,
				'crm'        => true,
				'lang_id'    => $data['lang_id'],
				'password'   => Hash::make($data['password']),
			]
		);
	}
}
