<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\Models\LanguageData;
use Exception;
use Illuminate\Contracts\Support\Renderable;

/**
 * Class MainController
 *
 * @package App\Http\Controllers
 */
class MainController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(): Renderable
    {
        return view('hotel.main');
    }

    public function translation()
    {
        $langs = Language::whereActive(true)->get();

        foreach ($langs as $v) {
            $languages = $langs;

            foreach ($languages as $v2) {
                try {
                    LanguageData::updateOrCreate(
                        [
                            'lang_id'     => $v->id,
                            'language_id' => $v2->id,
                        ],
                        [
                            'title' => ucfirst_utf8(googleTrans($v2->title, $v->const, 'en'))
                        ]
                    );
                } catch (Exception $exception) {
                    continue;
                }
            }
        }
    }
}
