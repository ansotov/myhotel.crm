<?php /** @noinspection PhpInconsistentReturnPointsInspection */

namespace App\Http\Controllers\Hotel;

use App\Http\Controllers\Controller;
use App\Jobs\Hotels\OfferDataTranslate;
use App\Models\Offers\Offer;
use App\Models\Offers\OfferData;
use App\Models\Offers\OfferImage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

/**
 * Class OffersController
 *
 * @package App\Http\Controllers\Hotel
 */
class OffersController extends Controller
{
    use SoftDeletes;

    private $tempImages = [];
    private $offersFolder;
    private $tempFolder;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->archive) {
            $this->items = Offer::archive()->paginate(setting('site.perpage'));
        } else {
            $this->items = Offer::hotelOffers()->paginate(setting('site.perpage'));
        }

        return view(
            'hotel.offers.items',
            [
                'items' => $this->items
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('hotel.offers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'       => 'required|array',
            'description' => 'required|array',
        ]);

        $request->request->add(['slug' => Str::slug($request->title[auth()->user()->language->id] . '-' . time())]);

        $request->validate([
            'slug' => 'required|unique:offers,slug,hotel_id',
        ]);

        $item = new Offer(
            [
                'hotel_id' => auth()->user()->hotel_id,
                'slug'     => $request->slug,
                'start_at' => $request->start_at,
                'end_at'   => $request->end_at,
                'active'   => isset($request['active']) ? 1 : 0,
            ]
        );
        $item->save();

        foreach (auth()->user()->hotel->languages as $v) {
            OfferData::updateOrCreate(
                [
                    'offer_id' => $item->id,
                    'lang_id'  => $v->id,
                ],
                [
                    'title'       => $request->title[$v->id],
                    'description' => $request->description[$v->id],
                    'text'        => $request->text[$v->id],
                ]
            );
        }

        $this->folders();
        $images = glob($this->tempFolder . DIRECTORY_SEPARATOR . '*');

        // Save images
        $this->saveImages($images, $item);

        $this->folders(true);

        // Queue translations
        OfferDataTranslate::dispatch($item, $request->all(), auth()->user()->language, auth()->user());

        return redirect(route('hotel.offers.index'))->with('status', __('forms.saved'));
    }

    /**
     * @param $images
     * @param $item
     */
    private function saveImages($images, $item)
    {
        foreach ($images as $v) {
            $imageName = File::name($v) . '.' . File::extension($v);

            // Save many types of images
            foreach ($this->imagesSizes as $k2 => $v2) {
                Image::make($v)->resize($v2, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($this->offersFolder . DIRECTORY_SEPARATOR . $k2 . $imageName, $this->imageQuality);
            }

            $image                   = new OfferImage();
            $image->image            = $imageName;
            $image->offer_id = $item->id;
            $image->save();
        }
    }

    /**
     * Folders
     *
     * @param bool $delete
     */
    private function folders($delete = false)
    {
        $this->tempFolder   = hotelFolder(md5(auth()->user()->hotel->id), 'temp');
        $this->offersFolder = hotelFolder(md5(auth()->user()->hotel->id), 'offers');

        if ($delete == true) {
            array_map('unlink', glob($this->tempFolder . '/*'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        return view('hotel.offers.edit', [
            'item' => Offer::find($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'title' => 'required',
        ]);

        $item           = Offer::find($id);
        $item->start_at = $request['start_at'];
        $item->end_at   = $request['end_at'];
        $item->active   = isset($request['active']) ? 1 : 0;

        $item->save();

        $this->folders();
        $images = glob($this->tempFolder . DIRECTORY_SEPARATOR . '*');

        // Save images
        $this->saveImages($images, $item);

        $this->folders(true);

        // Delete conditions
        OfferData::where(['offer_id' => $item->id])->delete();

        foreach ($this->hotelLanguages(auth()->user()->hotel_id) as $v) {
            $subItem = new OfferData(
                [
                    'lang_id'     => $v->lang_id,
                    'offer_id'    => $item->id,
                    'title'       => $request['title'][$v->lang_id],
                    'description' => $request['description'][$v->lang_id],
                    'text'        => $request['text'][$v->lang_id],
                ]
            );
            $subItem->save();
        }

        // Queue translations
        OfferDataTranslate::dispatch($item, $request->all(), auth()->user()->language, auth()->user());

        return redirect(route('hotel.offers.index'))->with('status', __('forms.edited'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(int $id)
    {
        Offer::where(['id' => $id])->delete();

        return redirect(route('hotel.offers.index'))->with('status', __('forms.deleted'));
    }

    /**
     * @param $id
     * @param $value
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function archive($id, $value)
    {
        Offer::where(['id' => $id])->update(
            [
                'archive' => $value,
            ]
        );

        $status = $value == true ? __('forms.in-archive') : __('forms.restored');

        return redirect(route('hotel.offers.index'))->with('status', $status);
    }
}
