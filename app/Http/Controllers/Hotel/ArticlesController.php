<?php

namespace App\Http\Controllers\Hotel;

use App\Http\Controllers\Controller;
use App\Jobs\Hotels\ArticleDataTranslate;
use App\Jobs\Hotels\ContactArticleDataTranslate;
use App\Models\Articles\Article;
use App\Models\Articles\ArticleData;
use App\Models\Articles\ContactArticle;
use App\Models\Articles\ContactArticleData;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Class ArticlesController
 *
 * @package App\Http\Controllers\Hotel
 */
class ArticlesController extends Controller
{
    use SoftDeletes;

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->archive) {
            $this->items = Article::archive()->paginate(setting('site.perpage'));
        } else {
            $this->items = Article::hotelArticles()->paginate(setting('site.perpage'));
        }

        return view('hotel.articles.items', ['items' => $this->items]);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('hotel.articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|array',
        ]);

        $request->request->add(['slug' => Str::slug($request->title[auth()->user()->language->id] . '-' . time())]);

        $request->validate([
            'slug' => 'required|unique:articles,slug,hotel_id',
        ]);

        $item = new Article(
            [
                'hotel_id'     => auth()->user()->hotel_id,
                'slug'         => $request->slug,
                'published_at' => $request['published_at'],
                'active'       => isset($request['active']) ? 1 : 0,
            ]
        );
        $item->save();

        foreach ($this->hotelLanguages(auth()->user()->hotel_id) as $v) {
            $subItem = new ArticleData(
                [
                    'lang_id'     => $v->lang_id,
                    'article_id'  => $item->id,
                    'title'       => $request->title[$v->lang_id],
                    'description' => $request->description[$v->lang_id],
                    'text'        => $request->text[$v->lang_id],
                ]
            );
            $subItem->save();
        }

        // Queue translations
        ArticleDataTranslate::dispatch($item, $request->all(), auth()->user()->language, auth()->user());

        return redirect(route('hotel.articles.index'))->with('status', __('forms.saved'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        return view('hotel.articles.edit', [
            'item' => Article::find($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'title' => 'required',
        ]);

        $item = Article::whereId($id)->first();
        $item->update(
            [
                'published_at' => $request['published_at'],
                'active'       => isset($request['active']) ? 1 : 0,
            ]
        );

        // Delete conditions
        ArticleData::where(['article_id' => $id])->delete();

        foreach ($this->hotelLanguages(auth()->user()->hotel_id) as $v) {
            $subItem = new ArticleData(
                [
                    'lang_id'     => $v->lang_id,
                    'article_id'  => $id,
                    'title'       => $request->title[$v->lang_id],
                    'description' => $request->description[$v->lang_id],
                    'text'        => $request->text[$v->lang_id],
                ]
            );
            $subItem->save();
        }

        // Queue translations
        ArticleDataTranslate::dispatch($item, $request->all(), auth()->user()->language, auth()->user());

        return redirect(route('hotel.articles.index'))->with('status', __('forms.edited'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contacts()
    {
        $this->item = ContactArticle::where(function ($query) {
            $query->hotelItem();
        })->first();

        return view('hotel.settings.contacts', [
            'item' => $this->item,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function contactsUpdate(
        Request $request
    ) {
        $this->item = ContactArticle::hotelItem()->first();

        if ($this->item) {
            // Delete conditions
            ContactArticleData::where(['article_id' => $this->item->id])->delete();

            foreach ($request->text as $k => $v) {
                try {
                    $item = new ContactArticleData(
                        [
                            'lang_id'    => $k,
                            'article_id' => $this->item->id,
                            'text'       => $v,
                        ]
                    );
                    $item->save();
                } catch (Exception $exception) {
                    echo $k . '----';
                    dd($exception->getMessage());
                }
            }

            // Queue translations
            ContactArticleDataTranslate::dispatch(
                $this->item,
                $request->all(),
                auth()->user()->language,
                auth()->user()
            );
        }

        return redirect(route('hotel.contacts'))->with('status', __('forms.edited'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(int $id)
    {
        Article::where(['id' => $id])->delete();

        return redirect(route('hotel.articles.index'))->with('status', __('forms.deleted'));
    }

    /**
     * @param $id
     * @param $value
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function archive($id, $value)
    {
        Article::where(['id' => $id])->update(
            [
                'archive' => $value,
            ]
        );

        $status = $value == true ? __('forms.in-archive') : __('forms.restored');

        return redirect(route('hotel.articles.index'))->with('status', $status);
    }
}
