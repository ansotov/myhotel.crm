<?php

namespace App\Http\Controllers\Hotel;

use App\Http\Controllers\Controller;
use App\Jobs\Hotels\NewDataTranslate;
use App\Models\News\NewData;
use App\Models\News\NewItem;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Class NewsController
 *
 * @package App\Http\Controllers\Hotel
 */
class NewsController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->archive) {
            $this->items = NewItem::archive()->paginate(setting('site.perpage'));
        } else {
            $this->items = NewItem::hotelNews()->paginate(setting('site.perpage'));
        }

        return view('hotel.news.items', ['items' => $this->items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('hotel.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|array',
        ]);

        $request->request->add(['slug' => Str::slug($request->title[auth()->user()->language->id] . '-' . time())]);

        $request->validate([
            'slug' => 'required|unique:news,slug,hotel_id',
        ]);

        $item = new NewItem(
            [
                'hotel_id' => auth()->user()->hotel_id,
                'slug'     => $request->slug,
                'published_at' => $request['published_at'],
                'finished_at'  => $request['finished_at'],
                'active'   => isset($request['active']) ? 1 : 0,
            ]
        );
        $item->save();

        foreach ($this->hotelLanguages(auth()->user()->hotel_id) as $v) {
            $subItem = new NewData(
                [
                    'lang_id'     => $v->lang_id,
                    'new_id'      => $item->id,
                    'title'       => $request->title[$v->lang_id],
                    'description' => $request->description[$v->lang_id],
                    'text'        => $request->text[$v->lang_id],
                ]
            );
            $subItem->save();
        }

        // Queue translations
        NewDataTranslate::dispatch($item, $request->all(), auth()->user()->language, auth()->user());

        return redirect(route('hotel.news.index'))->with('status', __('forms.saved'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        return view('hotel.news.edit', [
            'item' => NewItem::find($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'title' => 'required',
        ]);

        $item = NewItem::whereId($id)->first();
        $item->update(
            [
                'published_at' => $request['published_at'],
                'finished_at'  => $request['finished_at'],
                'active'       => isset($request['active']) ? 1 : 0,
            ]
        );

        // Delete conditions
        NewData::where(['new_id' => $id])->delete();

        foreach ($this->hotelLanguages(auth()->user()->hotel_id) as $v) {
            $subItem = new NewData(
                [
                    'lang_id'     => $v->lang_id,
                    'new_id'      => $id,
                    'title'       => $request->title[$v->lang_id],
                    'description' => $request->description[$v->lang_id],
                    'text'        => $request->text[$v->lang_id],
                ]
            );
            $subItem->save();
        }

        // Queue translations
        NewDataTranslate::dispatch($item, $request->all(), auth()->user()->language, auth()->user());

        return redirect(route('hotel.news.index'))->with('status', __('forms.edited'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(int $id)
    {
        NewItem::where(['id' => $id])->delete();

        return redirect(route('hotel.news.index'))->with('status', __('forms.deleted'));
    }

    /**
     * @param $id
     * @param $value
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function archive($id, $value)
    {
        NewItem::where(['id' => $id])->update(
            [
                'archive' => $value,
            ]
        );

        $status = $value == true ? __('forms.in-archive') : __('forms.restored');

        return redirect(route('hotel.news.index'))->with('status', $status);
    }
}
