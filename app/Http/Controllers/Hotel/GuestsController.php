<?php

namespace App\Http\Controllers\Hotel;

use App\Http\Controllers\Controller;
use App\Models\Hotels\HotelGuest;
use App\Models\Hotels\HotelGuestCode;
use App\Models\Hotels\HotelUser;
use App\Models\Rooms\Room;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

/**
 * Class GuestsController
 *
 * @package App\Http\Controllers\Hotel
 */
class GuestsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->trashed) {
            $this->items = HotelGuest::hotelGuests()->withTrashed();
        } elseif ($request->onlyTrashed) {
            $this->items = HotelGuest::hotelGuests()->onlyTrashed();
        } else {
            $this->items = HotelGuest::hotelGuests();
        }

        return view('hotel.guests.items', ['items' => $this->items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view(
            'hotel.guests.create',
            [
                'rooms' => Room::hotelRooms()->asc()->get(),
                'users' => HotelUser::where(['hotel_id' => auth()->user()->hotel_id])->get(),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $rules     = array(
            'start_at' => ['required', 'date'],
            'end_at'   => ['required', 'date'],
        );
        $validator = Validator::make($request->all(), $rules);

        if ($this->isFree($request) == false) {
            $validator->getMessageBag()->add('start_at', __('errors.bookedDates'));

            return redirect(route('hotel.guests.index'))->withErrors($validator);
        } else {
            $item = new HotelGuest([
                'room_id'  => $request->get('room_id'),
                'user_id'  => $request->get('user_id'),
                'start_at' => $request->get('start_at'),
                'end_at'   => $request->get('end_at'),
                'comment'  => $request->get('comment'),
            ]);
            $item->save();

            // Set access code
            $code = new HotelGuestCode([
                'guest_id' => $item->id,
                'code'     => $this->getCode(),
            ]);
            $code->save();

            return redirect(route('hotel.guests.index'))->with('status', __('forms.saved'));
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    private function isFree(Request $request): bool
    {
        $isset = HotelGuest::where(
            [
                'room_id'    => $request->room_id,
                'deleted_at' => null,
            ]
        )
            ->where(
                function ($query) use ($request) {
                    $query->whereBetween('start_at', [$request->start_at, $request->end_at])
                        ->orWhereBetween('end_at', [$request->start_at, $request->end_at]);
                }
            )
            ->first();

        return ! $isset;
    }

    /**
     * Code generator
     *
     * @return string
     */
    private function getCode(): string
    {
        start:
        $code = Str::random(10);

        $issetCode = HotelGuestCode::where(['code' => $code])->first();
        if ($issetCode) {
            goto start;
        } else {
            return $code;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        return view('hotel.guests.edit', [
            'item'  => HotelGuest::find($id),
            'rooms' => Room::hotelRooms()->asc()->get(),
            'users' => User::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, int $id)
    {
        $rules = array(
            'start_at' => ['required', 'date'],
            'end_at'   => ['required', 'date'],
        );

        $validator = Validator::make($request->all(), $rules);

        if ($this->isFree($request) == false) {
            $validator->getMessageBag()->add('start_at', __('errors.bookedDates'));

            return redirect(route('hotel.guests.index'))->withErrors($validator);
        } else {
            $item           = HotelGuest::find($id);
            $item->start_at = $request->get('start_at');
            $item->end_at   = $request->get('end_at');
            $item->comment  = $request->get('comment');

            $item->save();

            return redirect(route('hotel.guests.index'))->with('status', __('forms.edited'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(int $id)
    {
        HotelGuest::where(['id' => $id])->delete();

        return redirect(route('hotel.guests.index', ['onlyTrashed' => true]))->with('status', __('forms.deleted'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function restore(int $id)
    {
        HotelGuest::where(['id' => $id])->restore();

        return redirect(route('hotel.guests.index', ['trashed' => true]))->with('status', __('forms.restored'));
    }
}
