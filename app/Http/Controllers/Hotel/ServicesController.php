<?php

namespace App\Http\Controllers\Hotel;

use App\Http\Controllers\Controller;
use App\Jobs\Hotels\ServiceDataTranslate;
use App\Mail\TextMail;
use App\Models\Hotels\HotelService;
use App\Models\Hotels\HotelServiceData;
use App\Models\Hotels\HotelServiceImage;
use App\Models\Services\ServiceType;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

/**
 * Class ServicesController
 *
 * @package App\Http\Controllers\Hotel
 */
class ServicesController extends Controller
{

    /**
     * @var array
     */
    private $tempImages = [];

    /**
     * @var string
     */
    private $servicesFolder;

    /**
     * @var string
     */
    private $tempFolder;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->archive) {
            $this->items = HotelService::archive();
        } else {
            $this->items = HotelService::hotelItems();
        }

        return view('hotel.services.items', ['items' => $this->items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->folders(true);

        return view(
            'hotel.services.create',
            [
                'types'      => ServiceType::all(),
                'currencies' => auth()->user()->hotel->currencies,
            ]
        );
    }

    /**
     * Folders
     *
     * @param bool $delete
     */
    private function folders($delete = false)
    {
        $this->tempFolder     = hotelFolder(md5(auth()->user()->hotel->id), 'temp');
        $this->servicesFolder = hotelFolder(md5(auth()->user()->hotel->id), 'services');

        if ($delete == true) {
            array_map('unlink', glob($this->tempFolder . '/*'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'       => 'required|array',
            'type_id'     => 'required',
            'currency_id' => 'required',
        ]);

        $request->request->add(
            [
                'slug' => Str::slug($request->title[auth()->user()->language->id] . '-'
                    . $request->hotel_id . $request->type_id)
            ]
        );

        $request->validate([
            'slug' => 'required|unique:hotel_services,slug,hotel_id',
        ]);

        $item = new HotelService([
            'hotel_id'    => auth()->user()->hotel_id,
            'type_id'     => (int)$request->type_id,
            'price'       => $request->price,
            'currency_id' => $request->currency_id,
            'slug'        => $request->slug,
            'active'      => (bool)$request->active,
        ]);
        $item->save();

        foreach (auth()->user()->hotel->languages as $v) {
            HotelServiceData::updateOrCreate(
                [
                    'service_id' => $item->id,
                    'lang_id'    => $v->id,
                ],
                [
                    'title'       => $request->title[$v->id],
                    'description' => $request->description[$v->id],
                ]
            );
        }

        $this->folders();
        $images = glob($this->tempFolder . DIRECTORY_SEPARATOR . '*');

        foreach ($images as $v) {
            $imageName = File::name($v) . '.' . File::extension($v);

            // Save many types of images
            foreach ($this->imagesSizes as $k2 => $v2) {
                Image::make($v)->resize($v2, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($this->servicesFolder . DIRECTORY_SEPARATOR . $k2 . $imageName, $this->imageQuality);
            }

            $image                   = new HotelServiceImage();
            $image->image            = $imageName;
            $image->hotel_service_id = $item->id;
            $image->save();
        }

        $this->folders(true);

        // Queue translations
        ServiceDataTranslate::dispatch($item, $request->all(), auth()->user()->language, auth()->user());

        return redirect(route('hotel.services.index'))->with('status', __('forms.saved'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $this->folders(true);

        return view(
            'hotel.services.edit',
            [
                'item'       => HotelService::find($id),
                'types'      => ServiceType::all(),
                'currencies' => auth()->user()->hotel->currencies,
            ]
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'       => 'required',
            'type_id'     => 'required',
            'price'       => 'required',
            'currency_id' => 'required',
        ]);

        $item              = HotelService::find($id);
        $item->type_id     = $request->type_id;
        $item->price       = $request->price;
        $item->currency_id = $request->currency_id;
        $item->active      = (bool)$request->active;

        $item->save();

        $this->folders();
        $images = glob($this->tempFolder . DIRECTORY_SEPARATOR . '*');

        if ($images) {
            foreach ($images as $v) {
                $imageName = File::name($v) . '.' . File::extension($v);

                // Save many types of images
                foreach ($this->imagesSizes as $k2 => $v2) {
                    Image::make($v)->resize($v2, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($this->servicesFolder . DIRECTORY_SEPARATOR . $k2 . $imageName, $this->imageQuality);
                }

                $image                   = new HotelServiceImage();
                $image->image            = $imageName;
                $image->hotel_service_id = $item->id;
                $image->save();
            }
        }

        $this->folders(true);

        // Queue translations
        ServiceDataTranslate::dispatch($item, $request->all(), auth()->user()->language, auth()->user());

        return redirect(route('hotel.services.index'))->with('status', __('forms.saved-and-in-queue'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(int $id)
    {
        HotelService::where(['id' => $id])->delete();

        return redirect(route('hotel.services.index'))->with('status', __('forms.deleted'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return false|string
     * @noinspection PhpInconsistentReturnPointsInspection
     */
    public function destroyImage(Request $request)
    {
        if (auth()->user()->hotel->id != $request->hotel_id) {
            return false;
        }

        try {
            $image    = HotelServiceImage::where(['id' => $request->id])->first();
            $imageUrl = $image->image;
            $image->delete();

            $this->folders();
            foreach ($this->imagesSizes as $k2 => $v2) {
                unlink($this->servicesFolder . DIRECTORY_SEPARATOR . $k2 . $imageUrl);
            }
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * @param $id
     * @param $value
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function archive($id, $value)
    {
        HotelService::where(['id' => $id])->update(
            [
                'archive' => $value,
            ]
        );

        $status = $value == true ? __('forms.in-archive') : __('forms.restored');

        return redirect(route('hotel.services.index'))->with('status', $status);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function imageUploader(Request $request): JsonResponse
    {
        request()->validate([
            'images.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4048',
        ]);

        if ($files = $request->file('images')) {
            $this->folders();

            foreach ($files as $k => $v) {
                // Upload Original Image
                $image = md5(date('YmdHis') . $k) . "." . $v->getClientOriginalExtension();
                $v->move($this->tempFolder, $image);

                $this->tempImages[$k] = tempFolderPath('hotels' . DIRECTORY_SEPARATOR . md5(auth()->user()->hotel->id))
                    . DIRECTORY_SEPARATOR . $image;
            }
        }

        return response()->json(
            [
                'images'  => $this->tempImages,
                'success' => true
            ]
        );
    }
}
