<?php

namespace App\Http\Controllers\Hotel;

use App\Http\Controllers\Controller;
use App\Models\Hotels\HotelUser;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

/**
 * Class UsersController
 *
 * @package App\Http\Controllers\Hotel
 */
class UsersController extends Controller
{
	/**
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
	 */
	public function create(): View
	{
		return view('hotel.users.ajax.addUser');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return bool
	 */
	public function store(Request $request): bool
	{
		$request->validate([
			'name'    => 'required',
			'phone'   => 'required',
			'email'   => 'required',
			'lang_id' => 'required',
		]);

		$request->name  = trim($request->name);
		$request->phone = str_replace(' ', '', $request->phone);
		$request->email = trim($request->email);

		$user = User::firstOrCreate(
			[
				'phone' => $request->phone
			],
			[
				'lang_id'  => $request->lang_id,
				'role_id'  => $this->role('user')->id,
				'name'     => $request->name,
				'phone'    => $request->phone,
				'email'    => $request->email,
				'password' => str_random(8),
			]
		);


		$request->validate([
			'phone' => 'required|unique:hotel_users,phone,hotel_id',
			'email' => 'required|unique:hotel_users,email,hotel_id',
		]);

		HotelUser::firstOrCreate(
			[
				'phone' => $request->phone
			],
			[
				'user_id'       => $user->id,
				'hotel_id'      => auth()->user()->hotel_id,
				'name'          => $request->name,
				'phone'         => $request->phone,
				'email'         => $request->email,
				'personal_data' => $request->personal_data,
			]
		);

		return true;
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \App\Models\Hotels\HotelUser|\Illuminate\Database\Eloquent\Collection
	 */
	public function searchUser(Request $request): HotelUser|Collection
	{
		$str = trim($request->term);

		return HotelUser::select('id', 'name', 'phone', 'email')
			->where('hotel_id', auth()->user()->hotel_id)
			->where(
				function ($query) use ($str) {
					$query->where('name', 'like', '%' . $str . '%')
						->orWhere('phone', 'like', '%' . $str . '%')
						->orWhere('email', 'like', '%' . $str . '%');
				}
			)
			->get();
	}

	/**
	 * @param int $id
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function destroy(int $id)
	{
		$user = User::whereId($id)->first();

		if ($this->checkUser($user) === true) {
			$user->delete();

			return redirect(route('hotel.crm-users.index'))->with('success', __('forms.deleted'));
		} else {
			return redirect(route('hotel.crm-users.index'))->with('status', __('forms.not-enough-permissions'));
		}
	}

	/**
	 * @param \App\Models\User $user
	 *
	 * @return bool
	 */
	private function checkUser(User $user): bool
	{
		if (
			$user->id === auth()->user()->id ||
			$user->hotel_id !== auth()->user()->hotel_id ||
			auth()->user()->hotel_main != true
		) {
			return false;
		} else {
			return true;
		}
	}
}
