<?php

namespace App\Http\Controllers\User\Profile;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class SettingsController
 *
 * @package App\Http\Controllers\User\Profile
 */
class SettingsController extends Controller
{
    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('user.profile.settings', ['item' => auth()->user()]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request): RedirectResponse
    {
        $request->validate([
            'lang_id' => 'required',
            'name'    => 'required',
            'phone'    => 'required',
        ]);

        $item          = User::find(auth()->user()->id);
        $item->lang_id = $request->lang_id;
        $item->name    = $request->name;
        $item->phone   = $request->phone;

        $item->save();

        return back()->with('status', __('forms.edited'));
    }
}
