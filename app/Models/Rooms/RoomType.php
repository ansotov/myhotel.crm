<?php

namespace App\Models\Rooms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

/**
 * Class RoomType
 *
 * @package App\Models\Rooms
 */
class RoomType extends Model
{
    /**
     * Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function data(): BelongsTo
    {
        return $this->belongsTo(
            'App\Models\Rooms\RoomTypeData',
            'id',
            'room_type_id'
        )
            ->where(['lang_id' => Auth::user()->lang_id]);
    }
}
