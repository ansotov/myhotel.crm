<?php

namespace App\Models\Services;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Service
 *
 * @package App\Models\Servicess
 */
class Service extends Model
{
    /**
     * @var string[]
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'hotel_id',
        'slug',
        'active',
    ];

    /**
     * @var array
     */
    private $serviceIds = [];

    /**
     * Type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo('App\Models\Services\ServiceType', 'type_id', 'id');
    }

    /*
     * Filters
     */

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('active', 1);
    }

    /**
     * Sorting.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $field
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function scopeAscItems(Builder $query, $field = 'title'): LengthAwarePaginator
    {
        foreach (auth()->user()->services as $v) {
            $this->serviceIds[] = $v->service_id;
        }

        return $query->leftJoin('service_data', 'service_data.service_id', '=', 'services.id')
            ->select('services.*')
            ->where('service_data.lang_id', auth()->user()->lang_id)
            ->whereNotIn('services.id', $this->serviceIds)
            ->orderBy('service_data.' . $field)
            ->paginate(setting('site.perpage'));
    }

    /**
     * Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function data(): BelongsTo
    {
        return $this->belongsTo(
            'App\Models\Services\ServiceData',
            'id',
            'service_id'
        )
            ->where(['lang_id' => auth()->user()->lang_id]);
    }
}
