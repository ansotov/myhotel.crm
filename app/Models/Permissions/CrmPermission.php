<?php

namespace App\Models\Permissions;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CrmPermission extends Model
{
	use HasFactory;

	/**
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * @param int|null $langId
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function data(int $langId = null): BelongsTo
	{
		$langId = $langId ?? auth()->user()->lang_id;

		return $this->belongsTo(
			'App\Models\Permissions\CrmPermissionData',
			'id',
			'permission_id'
		)
			->where(['lang_id' => $langId]);
	}

	/**
	 * @param \Illuminate\Database\Query\Builder $query
	 * @param                                    $route
	 *
	 * @return object
	 */
	public function scopeHasAccess($query, $route)
	{
		return $query->join('crm_permission_roles', 'crm_permissions.id', '=', 'crm_permission_roles.permission_id')
			->select('crm_permissions.id')
			->where('crm_permissions.route', $route)
			->where('crm_permission_roles.role_id', auth()->user()->crm_role_id)
			->first();
	}
}
