<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class NewItem
 *
 * @package App\Models\News
 * @method static archive()
 * @method static hotelNews()
 * @method static find(int $id)
 * @method static whereId(int $id)
 * @method static where(int[] $array)
 */
class NewItem extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'news';

    /**
     * @var string[]
     */
    protected $dates = [
        'published_at',
        'deleted_at'
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'hotel_id',
        'slug',
        'published_at',
        'finished_at',
        'active',
    ];

    /**
     * @param int|null $langId
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function data(int $langId = null): BelongsTo
    {
        $langId = $langId ?? auth()->user()->lang_id;

        return $this->belongsTo(
            'App\Models\News\NewData',
            'id',
            'new_id'
        )
            ->where(['lang_id' => $langId]);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeArchive(Builder $query): Builder
    {
        return $query->where(['archive'=> true, 'hotel_id' => auth()->user()->hotel_id]);
    }
}
