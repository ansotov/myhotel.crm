<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Language
 *
 * @package App\Models
 * @method static where(array $array)
 */
class Language extends Model
{
    /**
     * @var string
     */
    protected $connection = 'mysql_geo';

    /**
     * Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function data(): BelongsTo
    {
        $langId = auth()->check() ? auth()->user()->language->id : Language::where(
            [
                'const' => app()->getLocale()
            ]
        )
            ->first()->id;
        return $this->belongsTo(
            'App\Models\LanguageData',
            'id',
            'language_id'
        )
            ->where(
                [
                    'lang_id' => $langId
                ]
            );
    }
}
