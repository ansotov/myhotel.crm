<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class OrderStatus
 *
 * @package App\Models\Orders
 */
class OrderStatus extends Model
{
    /**
     * @param int|null $langId
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function data(int $langId = null): BelongsTo
    {
        $langId = $langId ?? auth()->user()->lang_id;

        return $this->belongsTo(
            'App\Models\Orders\OrderStatusData',
            'id',
            'status_id'
        )
            ->where(['lang_id' => $langId]);
    }
}
