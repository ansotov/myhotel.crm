<?php

namespace App\Models\Orders;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Order
 *
 * @package App\Models\Orders
 * @method static hotelOrders()
 * @method static whereId($id)
 * @method static find($id)
 * @method static where($id)
 * @method static filter($array)
 */
class Order extends Model
{
    use SoftDeletes;
    use Filterable;

    /**
     * @var string[]
     */
    protected $dates = [
        'published_at',
        'deleted_at'
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'hotel_id',
        'user_id',
        'status_id',
    ];

    /**
     * Products
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products(): HasMany
    {
        return $this->hasMany('App\Models\Orders\OrderProduct', 'order_id', 'id');
    }

    /**
     * Status
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status(): BelongsTo
    {
        return $this->belongsTo('App\Models\Orders\OrderStatus', 'status_id', 'id');
    }

    /**
     * User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    /**
     * Room
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room(): BelongsTo
    {
        return $this->belongsTo('App\Models\Rooms\Room', 'room_id', 'id');
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeHotelOrders($query)
    {
        return $query->where('orders.hotel_id', auth()->user()->hotel_id)
			->orderBy('created_at', 'desc');
    }
}
