<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class HotelService
 *
 * @package App\Models\Hotels
 * @method static archive()
 * @method static hotelItems()
 * @method static find(int $id)
 * @method static where(int[] $array)
 */
class HotelService extends Model
{
    /**
     * @var string[]
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'hotel_id',
        'service_id',
        'type_id',
        'price',
        'currency_id',
        'slug',
        'active',
    ];

    /**
     * Type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo('App\Models\Services\ServiceType', 'type_id', 'id');
    }


    /**
     * @param int|null $langId
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function data(int $langId = null): BelongsTo
    {
        $langId = $langId ?? auth()->user()->lang_id;

        return $this->belongsTo(
            'App\Models\Hotels\HotelServiceData',
            'id',
            'service_id'
        )
            ->where(['lang_id' => $langId]);
    }

    /**
     * Images
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images(): HasMany
    {
        return $this->hasMany('App\Models\Hotels\HotelServiceImage', 'hotel_service_id', 'id');
    }

    /*
     * Filters
     */

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('active', 1);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeHotelItems($query)
    {
        return $query->join('hotel_service_data', 'hotel_service_data.service_id', '=', 'hotel_services.id')
            ->select('hotel_services.*')
            ->where('hotel_service_data.lang_id', auth()->user()->lang_id)
            ->where('hotel_id', auth()->user()->hotel_id)
            ->where('archive', false)
            ->paginate(setting('site.perpage'));
    }

    /**
     * Scope a query to only include active users.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeArchive($query)
    {
        return $query->join('hotel_service_data', 'hotel_service_data.service_id', '=', 'hotel_services.id')
            ->select('hotel_services.*')
            ->where('hotel_service_data.lang_id', auth()->user()->lang_id)
            ->where('hotel_id', auth()->user()->hotel_id)
            ->where('archive', true)
            ->paginate(setting('site.perpage'));
    }

    /**
     * Sorting.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $field
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAsc(Builder $query, $field = 'title'): Builder
    {
        return $query->leftJoin('hotel_service_data', 'hotel_service_data.service_id', '=', 'hotel_services.id')
            ->orderBy('hotel_service_data.' . $field);
    }
}
