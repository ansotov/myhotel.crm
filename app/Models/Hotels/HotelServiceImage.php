<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;

/**
 * Class HotelServiceImage
 *
 * @package App\Models\Hotels
 * @method static where(array $array)
 */
class HotelServiceImage extends Model
{
    //
}
