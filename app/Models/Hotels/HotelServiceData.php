<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;

/**
 * Class HotelServiceData
 *
 * @package App\Models\Hotels
 * @method static updateOrCreate(array $array, array $array1)
 */
class HotelServiceData extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'service_id',
        'lang_id',
        'title',
        'description',
    ];
}
