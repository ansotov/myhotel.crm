<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

/**
 * Class HotelOrderStatus
 *
 * @package App\Models\Hotels
 */
class HotelOrderStatus extends Model
{

    /**
     * Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function data(): BelongsTo
    {
        return $this->belongsTo(
            'App\Models\Hotels\HotelStatusData',
            'id',
            'status_id'
        )
            ->where(['lang_id' => Auth::user()->lang_id]);
    }
}
