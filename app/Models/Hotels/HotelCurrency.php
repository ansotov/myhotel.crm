<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class HotelCurrency
 *
 * @package App\Models\Hotels
 * @method static create(array $array)
 * @method static where(array $array)
 */
class HotelCurrency extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'hotel_id',
        'currency_id',
    ];

    /**
     * Currency
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo('App\Models\Currency', 'id', 'currency_id');
    }
}
