<?php

namespace App\Models\Logs;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EventType
 *
 * @package App\Models\Logs
 */
class EventType extends Model
{
    /**
     * @var string
     */
    protected $connection = 'mysql_logs';
}
