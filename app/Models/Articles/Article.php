<?php

namespace App\Models\Articles;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Article
 *
 * @package App\Models\Articles
 * @method static archive()
 * @method static hotelArticles()
 * @method static find(int $id)
 * @method static whereId(int $id)
 * @method static where(int[] $array)
 */
class Article extends Model
{
    /**
     * @var string[]
     */
    protected $dates = ['published_at', 'deleted_at'];

    /**
     * @var string[]
     */
    protected $fillable = [
        'hotel_id',
        'slug',
        'published_at',
        'active',
    ];

    /**
     * @param int|null $langId
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function data(int $langId = null): BelongsTo
    {
        $langId = $langId ?? auth()->user()->lang_id;

        return $this->belongsTo(
            'App\Models\Articles\ArticleData',
            'id',
            'article_id'
        )
            ->where(['lang_id' => $langId]);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeArchive($query)
    {
        return $query->where(['archive' => true, 'hotel_id' => auth()->user()->hotel_id]);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHotelArticles(Builder $query): Builder
    {
        return $query->where(['archive' => false, 'hotel_id' => auth()->user()->hotel_id]);
    }
}
