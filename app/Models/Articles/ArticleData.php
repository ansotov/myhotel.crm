<?php

namespace App\Models\Articles;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(int[] $array)
 */

/**
 * Class ArticleData
 *
 * @package App\Models\Articles
 * @method static where(int[] $array)
 */
class ArticleData extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'lang_id',
        'article_id',
        'title',
        'description',
        'text',
    ];
}
